import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'popmenu',
  templateUrl: './popmenu.component.html',
  styleUrls: ['./popmenu.component.scss']
})
export class PopmenuComponent implements OnInit {
  openMenu: boolean = false;

  constructor(public navCtrl: NavController) { }

  ngOnInit() {
  }

  togglePopupMenu() {
    return this.openMenu = !this.openMenu;
  } 

  togglePopupClose() {
    return this.openMenu = false;
  } 

  // // //
  receipt() {
    this.navCtrl.navigateForward('receipt');
    this.togglePopupClose();
  }

  scanQr() {
    // this.navCtrl.navigateRoot('charts');
    this.navCtrl.navigateForward('qr');
    this.togglePopupClose();
  }

  analytic() {
    // this.navCtrl.navigateRoot('charts');
    this.navCtrl.navigateForward('charts');
    this.togglePopupClose();
  }

  wallet() {
    // this.navCtrl.navigateRoot('charts');
    this.navCtrl.navigateForward('wallet');
    this.togglePopupClose();
  }

  home() {
    this.navCtrl.navigateForward('home');
    this.togglePopupClose();

  }

}
