import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { Service } from '../../../settings/Laravel';


@Injectable()
export class AuthProvider {

  isAuthenticated = false;

  constructor(public http: HttpClient, private storage: Storage){ }

  async checkIsAuthenticated(){
    let now = Date.now();
    let auth: any = await this.storage.get('auth');
    if (!!!auth) { return false; }
    if (auth.expired_at <= now){ return false; }
    return true;
  }

  login (user: any) {
    let request = {
      'grant_type': 'password',
      'client_id': Service.passport.client_id,
      'client_secret': Service.passport.client_secret,
      'username': user.email,
      'password': user.password,
    };
    return this.http.post(`https://www.api.eeceipt.info/oauth/token`, request).toPromise();
  }

  register (user: any) {
    return this.http.post(`https://www.api.eeceipt.info/api/v1/register`, user).toPromise();
  }

  removeCredentials () {
    this.storage.remove('auth');
  }

  storeCredentials (response: any) {

    let expired_at = (response.expires_in * 1000) + Date.now();

    this.storage.set('auth', {
      access_token: response.access_token,
      refresh_token: response.refresh_token,
      expired_at


    });
  
}

async fb_logout(userid:string, token:string){
    let headers: HttpHeaders = new HttpHeaders({  })
  let getR = this.http.delete(`https://graph.facebook.com/` + userid + '/permissions?access_token=' + token,{headers}).toPromise();
  return getR;
}

}
