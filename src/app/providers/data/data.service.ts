import { Injectable } from '@angular/core';
import { ToastController, LoadingController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private toastctrl : ToastController) { }

  company : any = 
  [{"id":1, "lat":"3.084181", "lng":"101.443238", "title":"Mydin Mart", "label":"3"},
   {"id":2, "lat":"3.083966", "lng":"101.447100", "title":"Warung Kita", "label":"3"},
   {"id":3, "lat":"3.078878", "lng":"101.438409", "title":"Family Mart", "label":"3"}]
  
  reward : any = [{"id":1, "compid":1, "title":"Rewards", "date":"2019-07-20", "content":"70% Discount"},
                  {"id":2, "compid":2, "title":"Rewards", "date":"2019-07-20", "content":"Buy One Free One"},
                  {"id":3, "compid":3, "title":"Rewards", "date":"2019-07-20", "content":"10% Discount"},
                  {"id":4, "compid":1, "title":"Rewards", "date":"2019-07-01", "content":"10% Discount"},
                  {"id":5, "compid":2, "title":"Rewards", "date":"2019-07-01", "content":"Buy One Free One"},
                  {"id":6, "compid":3, "title":"Rewards", "date":"2019-07-01", "content":"20% Discount"},
                  {"id":7, "compid":1, "title":"Rewards", "date":"2019-07-01", "content":"30% Discount"},
                  {"id":8, "compid":2, "title":"Rewards", "date":"2019-07-01", "content":"40% Discount"},
                  {"id":9, "compid":3, "title":"Rewards", "date":"2019-07-01", "content":"50% Discount"},
                  {"id":10, "compid":1, "title":"Rewards", "date":"2019-07-01", "content":"60% Discount"},
                  {"id":11, "compid":2, "title":"Rewards", "date":"2019-07-01", "content":"70% Discount"},
                  {"id":12, "compid":3, "title":"Rewards", "date":"2019-07-01", "content":"80% Discount"},
                  {"id":13, "compid":1, "title":"Rewards", "date":"2019-07-01", "content":"90% Discount"}];

  receipt : any = [{"id":1, "compid":1, "date":"2019-07-20", "price":"MYR23.60"},
                  {"id":2, "compid":2, "date":"2019-07-20", "price":"MYR23.60"},
                  {"id":3, "compid":3, "date":"2019-07-20", "price":"MYR23.60"},
                  {"id":4, "compid":1, "date":"2019-07-01", "price":"MYR23.60"},
                  {"id":5, "compid":2, "date":"2019-07-01", "price":"MYR23.60"},
                  {"id":6, "compid":3, "date":"2019-07-01", "price":"MYR23.60"},
                  {"id":7, "compid":1, "date":"2019-07-01", "price":"MYR23.60"},
                  {"id":8, "compid":2, "date":"2019-07-01", "price":"MYR23.60"},
                  {"id":9, "compid":3, "date":"2019-07-01", "price":"MYR23.60"},
                  {"id":10, "compid":1, "date":"2019-07-01", "price":"MYR23.60"},
                  {"id":11, "compid":2, "date":"2019-07-01", "price":"MYR23.60"},
                  {"id":12, "compid":3, "date":"2019-07-01", "price":"MYR23.60"},
                  {"id":13, "compid":1, "date":"2019-07-01", "price":"MYR23.60"}]                

  async presentToast(message:string){
    const toast = await this.toastctrl.create({ message:message, position: 'middle', duration: 2000 });
    toast.present();
  }  

}
