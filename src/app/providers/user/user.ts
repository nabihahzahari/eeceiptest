import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, LoadingController, NavController } from '@ionic/angular';
// import { Receipt } from '../../models/receipt';
import { Service } from '../../../settings/Laravel';
import { CanActivate } from '@angular/router';

@Injectable()
export class UserProvider implements CanActivate{

  constructor(public http: HttpClient, 
              private storage: Storage,
              private toastctrl: ToastController,
              private loadctrl: LoadingController,
              private navctrl: NavController) {
  }

  /*async canActivate(){
    this.storage.set["total"]=1;
    let loading = await this.loadctrl.create();
    await loading.present(); 
    let status : boolean;
    await this.storage.get("total").then( (val) =>{
       if(val==null){ this.storage.set("total",1); status=true; }
       else if(val==1){ this.navctrl.navigateForward('authentication'); status=false; }
    }) 
    await loading.dismiss();
    return status;
  }*/

  async canActivate(){
    let loading = await this.loadctrl.create();
    await loading.present(); 
    let now = Date.now();
    let auth: any = await this.storage.get('auth');
    await loading.dismiss();
    if (!!!auth) { return true; }
    if (auth.expired_at <= now){ return true; }
    await this.navctrl.navigateForward('home');
    return false; 
  }

  async getUserInfo(){
    let auth: any = await this.storage.get('auth');
    let headers: HttpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${auth.access_token}`,
    })
    let a = this.http.get(`${Service.url}/api/user`, { headers }).toPromise();
    return a;
  }

  async presentToast(msg:string) {
    const toast = await this.toastctrl.create({ message:msg, position: 'middle', duration: 2000 });
    toast.present();
  }    

  async getScanData ($guid, $id){
    let auth: any = await this.storage.get('auth');
    let headers: HttpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${auth.access_token}`,
    })
    let getScan = this.http.patch(`${Service.url}/api/v1/scan/` + $guid + '/' + $id, { headers }).toPromise();
    console.log(getScan);
    return getScan;
  }

  async sendRating(receipt_id:string, q1:number, q2:number, q3:number, comment:string) {
    let auth: any = await this.storage.get('auth');
    let headers: HttpHeaders = new HttpHeaders({
        'Authorization': `Bearer ${auth.access_token}`,
    })
    let data = {q1:q1, q2:q2, q3:q3, comment:comment};
    //let data = {q1:1, q2:1, q3:1, comment:"good food"};
    let getR = this.http.patch(`${Service.url}/api/v1` + 
               `/receipts/rating/` + receipt_id, data,
               { headers }).toPromise();
    return getR;
  }


  async getReceipt(id) {
    let auth: any = await this.storage.get('auth');
    let headers: HttpHeaders = new HttpHeaders({
        'Authorization': `Bearer ${auth.access_token}`,
      })
    let getR = this.http.get(`${Service.url}/api/v1/get/` + id, { headers }).toPromise();
    return getR;
  }

  async getHomeChart(id) {
    let auth: any = await this.storage.get('auth');
    let headers: HttpHeaders = new HttpHeaders({ 
        'Authorization': `Bearer ${auth.access_token}`,
      })
    // const headers = new HttpHeaders({
    //   'Authorization': this.token['token_type'] + ' ' + this.token['access_token']
    // });

    let getHR = this.http.get(`${Service.url}/api/v1/gethomechart/` + id, { headers }).toPromise();
    //  console.log(getHR);
    return getHR;

    // return this.http.get<Receipt>(this.env.API_URL + 'api/v1/receipts/get/' + id , { headers: headers })
    // .pipe(
    //   tap(receipt => {
    //     return receipt;

    //   }
    //   )

    // );
  }

}
