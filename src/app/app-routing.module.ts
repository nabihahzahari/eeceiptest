import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProvider } from './providers';

const routes: Routes = [
  { path: '', loadChildren: './pages/walkthrough/walkthrough.module#WalkthroughPageModule',  canActivate:[UserProvider] },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'edit-profile', loadChildren: './pages/edit-profile/edit-profile.module#EditProfilePageModule' },
 
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
  { path: 'support', loadChildren: './pages/support/support.module#SupportPageModule' },
 
  
  { path: 'authentication', loadChildren: './pages/authentication/authentication.module#AuthenticationPageModule', canActivate:[UserProvider]   },
  { path: 'popupmenu', loadChildren: './pages/popupmenu/popupmenu.module#PopupmenuPageModule' },
  { path: 'charts', loadChildren: './pages/charts/charts.module#ChartsPageModule' },
  { path: 'econfirm', loadChildren: './pages/econfirm/econfirm.module#EconfirmPageModule' },
  { path: 'receipt', loadChildren: './pages/receipt/receipt.module#ReceiptPageModule' },
  { path: 'qr', loadChildren: './pages/qr/qr.module#QrPageModule' },
  { path: 'rdetail', loadChildren: './pages/rdetail/rdetail.module#RdetailPageModule' },
//   { path: 'wallet', loadChildren: './pages/wallet/wallet.module#WalletPageModule' },
  { path: 'reward', loadChildren: './pages/reward/reward.module#RewardPageModule' },
  { path: 'location', loadChildren: './pages/location/location.module#LocationPageModule' },
  { path: 'rate', loadChildren: './pages/rate/rate.module#RatePageModule' },
  { path: 'walkthrough', loadChildren: './pages/walkthrough/walkthrough.module#WalkthroughPageModule', canActivate:[UserProvider] },
  { path: 'accountmodal', loadChildren: './pages/accountmodal/accountmodal.module#AccountmodalPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
