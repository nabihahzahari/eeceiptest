import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';
import { AgmCoreModule } from '@agm/core';
import { HomePage } from './home.page';
// import { PopmenuComponent } from './../../components/popmenu/popmenu.component';
import { QRCodeModule } from 'angular2-qrcode';
import { PopupmenuPageModule } from '../popupmenu/popupmenu.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QRCodeModule,
    PopupmenuPageModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    TranslateModule.forChild(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD9BxeSvt3u--Oj-_GD-qG2nPr1uODrR0Y'
    })
  ],
//   declarations: [HomePage, PopmenuComponent]
  declarations: [HomePage]
})
export class HomePageModule {}
