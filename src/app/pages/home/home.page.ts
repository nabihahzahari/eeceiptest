import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { NavController, MenuController, LoadingController, AlertController,Events } from '@ionic/angular';
import { TranslateProvider, AuthProvider, UserProvider } from '../../providers';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements AfterViewInit {

  @ViewChild('barCanvas') barCanvas;
  user:any; receipt:any; sum:0;
  id:any; value:any;

  constructor(    
    public menuCtrl: MenuController,
    public loadctrl: LoadingController,
    private translate: TranslateProvider,
    private authService: AuthProvider,
    private userService: UserProvider,
    private alertCtrl: AlertController,
    public events: Events,
    public navCtrl: NavController,
    private storage: Storage
  ) { }

ngAfterViewInit() {}

ngOnInit() {} 

async ionViewCanEnter () {
  let isAuthenticated = await this.authService.checkIsAuthenticated();
  return isAuthenticated;
}

//total:any;
ionViewWillEnter() {
  this.getUser();
  this.menuCtrl.enable(true);
  //this.storage.get("total").then( (val)=>{this.total=val;} )
}

async getUser (){
  let loading = await this.loadctrl.create();   
  await loading.present();
  this.userService.getUserInfo().then((response: any) => {
    this.user = response;
    this.id = response._id;
    this.value = response._id;
    this.events.publish('user', this.user);
  }).catch(err => {
    this.navCtrl.navigateRoot('authentication');
    this.userService.presentToast("Bad Connection");
  })
  await loading.dismiss();
}

editprofile(){ this.navCtrl.navigateForward('edit-profile'); }

async checkAuthenticated () {
    try {
      let isAuthenticated = await this.authService.checkIsAuthenticated();
      if ( isAuthenticated ) {
        this.menuCtrl.enable(true);
        this.navCtrl.navigateForward('/home');
      }
    } catch (err) {
      console.log(err);
      let alert = await this.alertCtrl.create({ header: 'Error', message: 'Error on verify authentication info', buttons: ['Ok'] });
      await alert.present();
    }
}

}
