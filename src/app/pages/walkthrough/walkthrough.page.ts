import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, IonSlides, MenuController, AlertController } from '@ionic/angular';
//import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-walkthrough',
  templateUrl: './walkthrough.page.html',
  styleUrls: ['./walkthrough.page.scss'],
})

export class WalkthroughPage implements OnInit {
    
  @ViewChild(IonSlides) slides: IonSlides;
  showSkip = true;
  slideOpts = { effect:'flip', speed: 1000 };

  slideList: Array<any> = [
    { title: 'What is <strong>Eeceipt</strong>?',
      description: 'We believe in transforming the traditional paper receipt',
      image: 'assets/img/hotel-sp01.png',
    },
    { title: 'Why Eeceipt?',
      description: 'Digitizing receipt will unlock multiple values for both retailer and customers.',
      image: 'assets/img/hotel-sp02.png',
    },
    { title: 'Your Eeceipt is coming!',
      description: ' Just scan the QR Code on the Simplified Eeceipt to unlock the level of details in the traditional paper receipt.',
      image: 'assets/img/qrcode-512.png',
    }
  ];

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public router: Router,
    public alertCtrl: AlertController,
    //private storage: Storage
  ) { }

  //auth:any;

  //view:number=0;
  ionViewWillEnter(){ 
    this.menuCtrl.enable(false); 
    //if(this.view==0){ this.navCtrl.navigateRoot('authentication'); }
    //this.auth=this.storage.get('auth');
  }
  //ionViewDidEnter(){ this.view=1; }

  ngOnInit(){ }

  onSlideNext(){ this.slides.slideNext(1000, false); }

	onSlidePrev(){ this.slides.slidePrev(300); }

  openLoginPage(){ this.navCtrl.navigateForward('/authentication'); }

}
