import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountmodalPage } from './accountmodal.page';

describe('AccountmodalPage', () => {
  let component: AccountmodalPage;
  let fixture: ComponentFixture<AccountmodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountmodalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountmodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
