import { Component, OnInit } from '@angular/core';
import { Platform, NavParams, ModalController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AuthProvider } from '../../providers';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-accountmodal',
  templateUrl: './accountmodal.page.html',
  styleUrls: ['./accountmodal.page.scss'],
})
export class AccountmodalPage implements OnInit {

  account : string;
  userdata : any =[];
  fbid:string; fbtoken:string;

  constructor(public navParams: NavParams,
              private storage: Storage,
              private loadctrl: LoadingController,
              private fb: Facebook,
              private modalctrl: ModalController,
              private authService: AuthProvider,
              private toastctrl: ToastController,
              private socialSharing: SocialSharing
              ) { }

  ngOnInit() { }

  async ionViewWillEnter() {
    this.userdata=[];
    this.account = this.navParams.data.account;
    if(this.account=="facebook"){ this.fbaccount(); }
    else if(this.account=="twitter"){ }
    else if(this.account=="googleplus"){ }
  }

  async fbaccount(){
    let loading = await this.loadctrl.create();
    await loading.present(); 
    await this.storage.get('fbuser').then((val) => {
      if(val==null || val==''){ this.userdata=[] }
      else{ this.userdata=val; }
    });
    await loading.dismiss();
  }

  async fblogin(){
    let loading = await this.loadctrl.create();
    await loading.present(); 
    await this.fb.login(['public_profile', 'user_friends', 'email']).then(async (res: FacebookLoginResponse) =>{
      await this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then( async profile => {
        this.userdata = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']};
        await this.storage.set('fbuser',this.userdata);
        this.fbid = res.authResponse.userID;
        this.fbtoken = res.authResponse.accessToken;
        this.presentToast("Successfully Log In");
      }).catch(e => this.presentToast("Backend Error") );   
    }).catch(e => alert("Error Login to Facebook") ) , { auth_type: 'reauthorize' }  
    await loading.dismiss();
  }

  async fblogout(){ 
    let loading = await this.loadctrl.create();
    await loading.present(); 
    await this.authService.fb_logout(this.fbid,this.fbtoken).then(async (response: any) => {
      if(response["success"]==true){ this.presentToast("Successfully Logged Out"); }
      else{ this.presentToast("Error Log Out"); }
    }).catch(err => { this.presentToast("Backend Error"); })
    await this.storage.remove('fbuser');
    await this.modalctrl.dismiss(); 
    await loading.dismiss();
  }

  async fbshare(){
    await this.socialSharing.shareViaFacebook('Body',null,null).then(() => {
      this.presentToast("success");
    }).catch(() => { this.presentToast("error"); });
  }

  async twittershare(){
    await this.socialSharing.shareViaTwitter('Body',null,null).then(() => {
      this.presentToast("success");
    }).catch(() => { this.presentToast("error"); });
  } 

  async presentToast(msg:string) {
    const toast = await this.toastctrl.create({ message:msg, position: 'middle', duration: 2000 });
    toast.present();
  }   

  close(){ this.modalctrl.dismiss(); }

}
