import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EconfirmPage } from './econfirm.page';

describe('EconfirmPage', () => {
  let component: EconfirmPage;
  let fixture: ComponentFixture<EconfirmPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EconfirmPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EconfirmPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
