import { Component, ViewChild, AfterViewInit } from '@angular/core';
import chartJs from 'chart.js';
import { NavController, LoadingController, MenuController, AlertController, Events } from '@ionic/angular';
import { TranslateProvider, HotelProvider, AuthProvider, UserProvider } from '../../providers';




@Component({
  selector: 'app-charts',
  templateUrl: './charts.page.html',
  styleUrls: ['./charts.page.scss'],
})
export class ChartsPage implements AfterViewInit {
  @ViewChild('barCanvas') barCanvas;
//   @ViewChild('doughnutCanvas') doughnutCanvas;


  barChart: any;
  user: any;
  sum: 0;
  id:any;

//   doughnutChart: any;


  constructor(
      public navCtrl: NavController,
       public menuCtrl: MenuController,
    private translate: TranslateProvider,
    public hotels: HotelProvider,
    private authService: AuthProvider,
    private userService: UserProvider,
    private alertCtrl: AlertController,
    public events: Events,
    ) {

     }

  ionViewWillEnter() {
        this.getUser();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.barChart = this.getBarChart(this.id);
    }, 450);

  }

  getUser ()
  {

    this.userService.getUserInfo()
      .then((response: any) => {
        //   console.log(response);
         this.user = response;
         this.id = response._id;
         this.getBarChart (this.id);
      })
      .catch(err => {
        this.navCtrl.navigateRoot('authentication');
      });
  }

  async ionViewCanEnter () {
    let isAuthenticated = await this.authService.checkIsAuthenticated();
    return isAuthenticated;
  }


  async checkAuthenticated () {
    try {
      let isAuthenticated = await this.authService.checkIsAuthenticated();
      if ( isAuthenticated ) {
        this.menuCtrl.enable(true);
        this.navCtrl.navigateForward('/home');
      }
    } catch (err) {
      console.log(err);
      let alert = await this.alertCtrl.create({ header: 'Error', message: 'Error on verify authentication info', buttons: ['Ok'] });
      await alert.present();
    }
  }

  getChart(context, chartType, data, options?) {
    return new chartJs(context, {
      data,
      options,
      type: chartType,
    });
  }



  async getBarChart(id) {
    const alert = await this.alertCtrl.create({
        message: 'Retrieving data, please wait...',
      });
      await alert.present();
    this.userService.getHomeChart(id)
        .then(async (response: any) => {
            await alert.dismiss();
    var Result = [];
    Object.keys(response).forEach(function(key) {
                Result.push({
                Months: key,
                Counts: Math.round((response[key]) * 100) / 100
                });
      });



var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
   'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
 ];
//  var startmonth = ['Dec', 'Nov', 'Oct', 'Sep', 'Aug', 'Jul',
//    'Jun', 'May', 'Apr', 'Mar', 'Feb', 'Jan'
//  ];

 var itemMonths = [],
itemCountList = [];
 var start;
//  var end = 11;
 var month;
 var year;

 var date = new Date();

 month = date.getMonth();
 year = parseInt('2019');
 start = 0;


 for (var i = 0; i < 12; i++) {
    var months = monthNames[start];
    var monthValue = 0;

    itemMonths.push(months);
    start = start + 1;
    if (start == 12) {
      start = 0;
      year = year + 1;
    }




    var dataObj = $.grep(Result, function(a) {
        return a.Months === months;
      })[0];
      monthValue = dataObj !== undefined ? dataObj.Counts : 0;
      itemCountList.push(monthValue);
    }

    const data = {
      labels: itemMonths,
      datasets: [{
        label: 'Total expenditure in ' + date.getFullYear() + ' (RM)' ,
        data: itemCountList,
        backgroundColor: [
          'rgba(255, 99, 132)',
          'rgba(54, 162, 235)',
          'rgba(255, 206, 86)',
          'rgba(75, 192, 192)',
          'rgba(153, 102, 255)',
          'rgba(255, 159, 64)',
          'rgba(255, 99, 132)',
          'rgba(54, 162, 235)',
          'rgba(255, 206, 86)',
          'rgba(75, 192, 192)',
          'rgba(153, 102, 255)',
          'rgba(255, 159, 64)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 3
      }]
    };

    const options = {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      };

      return this.getChart(this.barCanvas.nativeElement, 'bar', data, options);


  }).catch(err => {

    let alert = this.alertCtrl.create({ header: 'Error', message: 'Error searching', buttons: ['Ok'] });

  });
 }

}
