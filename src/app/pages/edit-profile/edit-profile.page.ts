import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController, Events, AlertController, MenuController } from '@ionic/angular';
import { TranslateProvider, AuthProvider, UserProvider } from '../../providers';
import { HomePage } from '../home/home.page';
import { FormGroup, FormBuilder } from '@angular/forms';



@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

    user: any;
    onUpdateForm: FormGroup;


  constructor(
    public navCtrl: NavController, 
    public loadingCtrl: LoadingController, 
    public toastCtrl: ToastController,
    private translate: TranslateProvider,
    public events: Events,
    private authService: AuthProvider,
    private userService: UserProvider,
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private menuctrl: MenuController) {  }

  ngOnInit() { }

  ionViewWillEnter() {
      this.getUser();
      this.inDev();
      this.menuctrl.close();
  }

  ionViewWillLeave  (){

  }

  
  getUser ()
  {
    // this.loading = true;
    this.userService.getUserInfo()
      .then((response: any) => {
        // this.loading = false;
        this.user = response;
        // console.log(this.user);
        //  console.log(this.user);
        // this.events.publish('user', this.user);

      })
      .catch(err => {
        this.navCtrl.navigateRoot('authentication');
        //   console.log('error');
        // this.loading = false;
        // let alert = this.alertCtrl.create({ title: 'Error', message: 'Error on get user info', buttons: ['Ok'] });
        // alert.present();
      })
  }

//   s () {
//     this.events.subscribe('userss', (response) => {
//         // user and time are the same arguments passed in `events.publish(user, time)`
//         // this.user.id = data.id;
//         // console.log(response);
//          this.user = response;
//          console.log('wei', this.user.name);
         
//       });
  
  
  async sendData(rawUpdate) {
      console.log(rawUpdate);

      
    // send booking info
    // const loader = await this.loadingCtrl.create({
    //   duration: 2000
    // });

    // loader.present();
    // loader.onWillDismiss().then(async l => {
    //   const toast = await this.toastCtrl.create({
    //     showCloseButton: true,
    //     cssClass: 'bg-profile',
    //     message: 'Your Data was Edited!',
    //     duration: 3000,
    //     position: 'bottom'
    //   });

    //   toast.present();
    //   this.navCtrl.navigateForward('/home');
    // });
    // end
  }

  async coming() {
    const alert = await this.alertCtrl.create({
      header: 'Stay Tuned!',
    //   subHeader: 'Subtitle',
      message: 'More features coming soon',
      buttons: ['OK']
    });

    await alert.present();
  }

  async inDev() {
    const alert = await this.alertCtrl.create({
      header: 'We are sorry',
      message: 'Page under construction',
    //   inputs: [
    //     {
    //       name: 'email',
    //       type: 'email',
    //       placeholder: this.translate.get('app.label.email')
    //     }
    //   ],
      buttons: [
        {
          text: 'Okay',
          handler: async () => {
            const loader = await this.loadingCtrl.create({
              duration: 100
            });

            loader.present();
            // loader.onWillDismiss().then(async l => {
            //   const toast = await this.toastCtrl.create({
            //     showCloseButton: true,
            //     message: this.translate.get('app.pages.login.text.sended'),
            //     duration: 3000,
            //     position: 'bottom'
            //   });

            //   toast.present();
            // });
          }
        }
      ]
    });

    await alert.present();
    
  }

}
