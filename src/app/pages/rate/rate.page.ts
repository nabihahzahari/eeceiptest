import { Component, OnInit } from '@angular/core';
import {  NavParams, LoadingController, ModalController, ToastController, NavController} from '@ionic/angular';
import { UserProvider } from '../../providers';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.page.html',
  styleUrls: ['./rate.page.scss'],
})
export class RatePage implements OnInit {

  /*star1 : string = "rated";
  star2 : string = "norate";
  star3 : string = "norate";
  star4 : string = "norate";
  star5 : string = "norate";

  name1 : string = "star";
  name2 : string = "star-outline";
  name3 : string = "star-outline";
  name4 : string = "star-outline";
  name5 : string = "star-outline";*/

  starmodal : number ;
  receiptId : string ;
  q1:number; q2:number; q3:number; comment:string;

  constructor( private loadctrl: LoadingController,
               private modalctrl: ModalController,
               private toastctrl: ToastController,
               private navParams: NavParams,
               private userService : UserProvider,
               private navctrl : NavController ) { }

  ngOnInit() {
    this.receiptId = this.navParams.data.receipt_id; 
    if(this.navParams.data.q1==null){ this.q1=0; }else{ this.q1 = this.navParams.data.q1; }
    if(this.navParams.data.q2==null){ this.q2=0; }else{ this.q2 = this.navParams.data.q2; }
    if(this.navParams.data.q3==null){ this.q3=0; }else{ this.q3 = this.navParams.data.q3; }
    if(this.navParams.data.comment==null){ this.comment=""; }
    else{ this.comment = this.navParams.data.comment; }
  }

  total:number=0;
  nombo:number=0;
  display:string="";

  /*async rate(num : number){
    let loading = await this.loadctrl.create();   
    await loading.present();

    this.star1="norate"; this.star2="norate"; this.star3="norate"; this.star4="norate"; this.star5="norate";

    if(num>=0){ this.star1="rated"; }
    if(num>=1){ this.star2="rated"; }
    if(num>=2){ this.star3="rated"; }
    if(num>=3){ this.star4="rated"; }
    if(num>=4){ this.star5="rated"; }

    await loading.dismiss();
  }*/

  close(){ this.modalctrl.dismiss(); }

  async submit(){ 
    let loading = await this.loadctrl.create();   
    await loading.present();
    await this.userService.sendRating(this.receiptId,this.q1,this.q2,this.q3,this.comment).then((response: any) => {
      if(response=="Success"){ this.presentToast("Thank You For Your Feedback"); }
      else{ alert("Error Submitting"); }
      //alert(JSON.stringify(response));
    }).catch(err => { alert("Backend Error"); })
    let data = { q1:this.q1, q2:this.q2, q3:this.q3, comment:this.comment };
    await this.modalctrl.dismiss(data).then(()=>{ //this.navctrl.navigateForward('receipt'); 
    });
    await loading.dismiss();
  }

  async presentToast(msg:string) {
    const toast = await this.toastctrl.create({ message:msg, position: 'middle', duration: 2000 });
    toast.present();
  }   

}
