import { Component, OnInit } from '@angular/core';
import { TranslateProvider, HotelProvider, AuthProvider, UserProvider } from '../../providers';
import { NavController, AlertController, ModalController, LoadingController } from '@ionic/angular';
import { RdetailPage } from '../rdetail/rdetail.page';
import { DataService } from '../../providers/data/data.service';


@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.page.html',
  styleUrls: ['./receipt.page.scss'],
})
export class ReceiptPage implements OnInit {
    user: any;
    id:any;
    receipt:any =[];
    display : any =[] ;  
    per_page : any=[];
    receiptdetail: any;
    inter: any;
     sum = 0;

     startdate : string = "2019-01-01";
     enddate : string = ( (new Date().toISOString()).split("T") )[0];   

     total_page : number =1;
     current_page : number = 1;
     company_list : any = [];
     choose_company : string = "0";

     star : number = 3 ;

  constructor( private userService: UserProvider , public navCtrl: NavController,  private alertCtrl: AlertController, 
    public modalController: ModalController, private loadCtrl: LoadingController,private data: DataService) { 
      
  }

  async ngOnInit() { }

  async next(){ 
    this.current_page=this.current_page+1; 
    await this.get_page();
  }

  async previous(){ 
    this.current_page=this.current_page-1; 
    await this.get_page();
  }
  
  async ionViewWillEnter() { 
    let loading = await this.loadCtrl.create();
    await loading.present(); 
    await this.getUser(); 
    await loading.dismiss();   
  }

  async getUser (){
    let loading = await this.loadCtrl.create();
    await loading.present(); 
    await this.userService.getUserInfo().then(async (response: any) => {
        let loading = await this.loadCtrl.create();
        await loading.present(); 
        this.user = response;
        this.id = response._id;
        await this.getOne (this.id);
        await loading.dismiss();
     }).catch(err => { 
        this.navCtrl.navigateRoot('authentication'); 
        this.userService.presentToast("Bad Connection");
     })
     await loading.dismiss();   
  }

  disp:string;
  async getOne (id) {
    await this.userService.getReceipt(id).then(async (response: any) => {
          this.receipt = response;
          this.display = response;
          this.count_page();
          this.get_company();
    }).catch(err => { let alert = this.alertCtrl.create({ header: 'Error', message: 'Error searching', buttons: ['Ok'] }); })
  }

  async getDetail(receiptdetail, html, q1, q2, q3, comment, _id) {
    const modal = await this.modalController.create({
      component: RdetailPage,
      componentProps: {
        'receiptId': receiptdetail,
        'html': html,
        'q1': q1,
        'q2': q2,
        'q3': q3,
        'comment': comment,
        '_id' : _id
      }
    });

    /*modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) { this.ionViewWillEnter(); }
    });*/

    return await modal.present();
  }

  async format_startdate(){
    this.startdate = (this.startdate.split("T"))[0];   
    await this.compare_date();
  }

  async format_enddate(){
    this.enddate = (this.enddate.split("T"))[0];
    await this.compare_date();
  }

  async reset(){ 
    let loading = await this.loadCtrl.create();
    await loading.present(); 
    this.display=this.receipt;
    this.choose_company = "0";
    this.startdate = "2019-01-01";
    this.enddate  = ( (new Date().toISOString()).split("T") )[0];
    await this.count_page(); 
    loading.dismiss();
  }

  async compare_date(){
    let loading = await this.loadCtrl.create();
    await loading.present(); 
    if(this.enddate < this.startdate){ 
      this.data.presentToast("End date must greater than start date");
      this.reset();
    }
    else{ 
      let temp : any = [];
      for(let a=0; a<this.receipt.length; a++){
        if( ( (this.receipt[a].date).substr(0, 19)>=this.startdate ) && 
            ( (this.receipt[a].date).substr(0, 19)<=this.enddate) ){
           await temp.push(this.receipt[a]);
        }
      }
      this.display=temp;
      await this.count_page();
    }
    await loading.dismiss();
  }

  count_page(){
    this.total_page = (this.display.length)/10;
    if( (this.display.length)%10 != 0 ){ this.total_page=this.total_page+1; }
    if( this.display.length==0 ){ this.total_page=1; }
    this.current_page=1;
    this.total_page=Number(this.total_page.toFixed());
    this.get_page();
  }

  async get_page(){
    let loading = await this.loadCtrl.create();
    await loading.present(); 
    let temp : any = [];
    let max = this.current_page*10;
    let min = max-10;
    for(let a=min; a<max; a++){
      if(this.display[a]!=null){ temp.push(this.display[a]); }
    }
    this.per_page=temp;
    loading.dismiss();
  }  

  async get_company(){
    let loading = await this.loadCtrl.create();
    await loading.present(); 
    let temp : any = [];
    for(let a=0; a<this.receipt.length; a++){
        let b=0
        for(; b<temp.length; b++){ if(this.receipt[a]["merchantId"]==temp[b]["merchantId"]){ break; } }
        if(b==temp.length){ temp.push(this.receipt[a]); }
    }
    this.company_list=temp;
    loading.dismiss();
  } 

  async filter_company(){
    this.startdate = "2019-01-01";
    this.enddate  = ( (new Date().toISOString()).split("T") )[0];
    let loading = await this.loadCtrl.create();
    await loading.present();
    if(this.choose_company!="0"){ 
      let temp : any = [];
      for(let a=0; a<this.receipt.length; a++ ){
         if(this.receipt[a]["merchantId"]==this.choose_company){ temp.push(this.receipt[a])}
      }
      this.display=temp;
      await this.count_page();
    }
    else{ this.reset(); }
    await loading.dismiss();
  }  

}
