import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReceiptPage } from './receipt.page';
import { PopupmenuPageModule } from '../popupmenu/popupmenu.module';


const routes: Routes = [
  {
    path: '',
    component: ReceiptPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopupmenuPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReceiptPage]
})
export class ReceiptPageModule {}
