import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { TranslateProvider, AuthProvider } from '../../providers';
import { decodeLaravelErrors } from '../../functions/Helpers';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.page.html',
  styleUrls: ['./authentication.page.scss'],
})
export class AuthenticationPage implements OnInit {
  onLoginForm: FormGroup;
  onRegisterForm: FormGroup;
  authSegment: String = 'login';
  loading: any;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private translate: TranslateProvider,
    private formBuilder: FormBuilder,
    private authService: AuthProvider,
    private fb: Facebook
  ){ }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
    //this.checkAuthenticated();
  }

  ngOnInit() {
    this.onLoginForm = this.formBuilder.group({
      'email': [null, Validators.compose([ Validators.required ])],
      'password': [null, Validators.compose([ Validators.required  ])]
    });
    this.onRegisterForm = this.formBuilder.group({
      'firstName': [ null, Validators.compose([Validators.required ]) ],
      'lastName': [ null, Validators.compose([ Validators.required ]) ],
      'email': [ null, Validators.compose([Validators.maxLength(70), Validators.email, Validators.required]) ],
      'password': [ null, Validators.compose([Validators.required]) ],
      'password_confirmation': [ null, Validators.compose([Validators.required ]) ]
    });
  } 

  async checkAuthenticated () {
    try {
      let isAuthenticated = await this.authService.checkIsAuthenticated();
      if ( isAuthenticated ) {
        this.menuCtrl.enable(true);
        this.navCtrl.navigateForward('/home');
      }
    } catch (err) {
      console.log(err);
      let alert = await this.alertCtrl.create({ header: 'Error', message: 'Error on verify authentication info', buttons: ['Ok'] });
      await alert.present();
    }
  }  

  async coming() {
    const alert = await this.alertCtrl.create({
      header: 'Stay Tuned!',
      message: 'More features coming soon',
      buttons: ['OK']
    });
    await alert.present();
  }

  async loadingCircle() {
    const alert = await this.alertCtrl.create({ message: 'Please wait...' });
    await alert.present();
  }

  async doLogin (rawLogin) {
    let data = JSON.stringify(rawLogin);
    let a = JSON.parse(data);
    const alert = await this.alertCtrl.create({ message: 'Logging in, please wait...' });
    await alert.present();
    this.authService.login(a).then(async (response: any) => {
      await alert.dismiss();
      this.authService.storeCredentials(response);
      setTimeout(() => {this.checkAuthenticated();}, 750);
    })
    .catch(async err => { 
      await alert.dismiss();

    const toast = await this.toastCtrl.create({
        message: '(Error Code:400) We are sorry, but it appears that there has been an internal server error. Our engineer have been notified and working to resolve the issue.',
        position: 'middle',
        duration: 5000,
        color: 'danger'


      });
      await alert.dismiss();

      const toasti = await this.toastCtrl.create({
        message: 'Unconfirmed / Incorrect e-Mail or Password, please try again.',
        showCloseButton: false,
        // closeButtonText: 'hide',
        position: 'middle',
        duration: 3000,
        color: 'danger'

      });
      await alert.dismiss();

      const toaste = await this.toastCtrl.create({
        message: '(Error Code:500) We are sorry, but it appears that there has been an internal server error. Our engineer have been notified and working to resolve the issue.',
        showCloseButton: false,
        // closeButtonText: 'hide',
        position: 'middle',
        duration: 5000,
        color: 'danger' 
      });

      const toasturrr = await this.toastCtrl.create({
        message: 'weiweiewei',
        showCloseButton: false,
        // closeButtonText: 'hide',
        position: 'middle',
        duration: 5000,
        color: 'danger' 
      });


      if  (err.status === 400) {
        toast.present();
      }
      if  (err.status === 401) {
        toasti.present();
      }
      if  (err.status === 500) {
        toaste.present();
      }
      if  (err.status === 503) {
        toasturrr.present();
      }      


    });


}


doRegister (rawReg)
{
  this.authService.register(rawReg)
     .then((response: any) => {
      this.navCtrl.navigateForward('econfirm');
     })
    .catch(async (err: any) => {
    let decodedErrors: any = decodeLaravelErrors(err);
    const toasta = await this.toastCtrl.create({

        message: decodedErrors.errors.join(' | '),
        showCloseButton: false,
        position: 'middle',
        duration: 6000,
        color: 'danger'


      });

      if  (err.status === 422) {
      toasta.present();
      }

    });

}

  //message : string = "kosong jek";

  /*doRegister (rawReg){
      this.authService.register(rawReg)
     .then( (response:any) => {
         this.navCtrl.navigateForward('econfirm');
         //alert("suda");
      })
     .catch(async (err:any) => {
         alert(JSON.stringify(err));
         let decodedErrors: any = decodeLaravelErrors(err);
         const toasta = await this.toastCtrl.create({
               message: decodedErrors.errors.join(' | '),
               showCloseButton: false,
               position: 'middle',
               duration: 6000,
               color: 'danger'
         });
         if (err.status === 422) { toasta.present(); }
        toasta.present();
     });
}*/


validateLoginData (data: any)
{
  return true;
}

/*userdata:any;
async fblogin(){
  let loading = await this.loadingCtrl.create();
  await loading.present(); 
  await this.fb.login(['public_profile', 'user_friends', 'email'])
    .then(async (res: FacebookLoginResponse) =>{
       //alert(JSON.stringify(email))
       await this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then( async profile => {
         this.userdata = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']};
         //let data = { email: profile['email'], password:'' }
       })  
    })
    .catch(e => alert("Error Login to Facebook") );    
  await this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);
  await this.loadingCtrl.dismiss();
}*/



}
