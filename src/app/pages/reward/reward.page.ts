import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ModalController, LoadingController } from '@ionic/angular';
import { DataService } from '../../providers/data/data.service';

@Component({
  selector: 'app-reward',
  templateUrl: './reward.page.html',
  styleUrls: ['./reward.page.scss'],
})
export class RewardPage implements OnInit {

  startdate : string = "2019-01-01";
  enddate : string = ( (new Date().toISOString()).split("T") )[0];
  total_page : number = 1;
  current_page : number = 1;

  reward : any = this.data.reward;
  display : any =[] ;  
  company_list : any = [];
  choose_company : number = 0;

  constructor(private alertCtrl: AlertController,
              private loadCtrl: LoadingController,
              private data: DataService) { }

  async ngOnInit(){
    await this.count_page();
    await this.get_company();
  }

  next(){ 
    this.current_page=this.current_page+1; 
    this.get_page();
  }

  previous(){ 
    this.current_page=this.current_page-1; 
    this.get_page();
  }  

  async reset(){ 
    this.reward = this.data.reward; 
    this.choose_company = 0;
    this.startdate = "2019-01-01";
    this.enddate  = ( (new Date().toISOString()).split("T") )[0];
    await this.count_page(); 
  }

  format_startdate(){
    this.startdate = (this.startdate.split("T"))[0];   
    this.compare_date(); 
  }

  format_enddate(){
    this.enddate = (this.enddate.split("T"))[0];
    this.compare_date();
  }

  async compare_date(){
    this.reward=this.data.reward;
    let loading = await this.loadCtrl.create();
    await loading.present(); 
    if(this.enddate < this.startdate){ 
      this.data.presentToast("End date must greater than start date");
      this.reset();
    }
    else{ 
      let temp : any = [];
      for(let a=0; a<this.reward.length; a++){
        if( (this.reward[a].date>=this.startdate) && (this.reward[a].date<=this.enddate) ){
           temp.push(this.reward[a]);
        }
      }
      this.reward=temp;
      this.choose_company=0;
      this.count_page();
    }
    loading.dismiss();
  }

  count_page(){
    this.total_page = (this.reward.length)/10;
    if( (this.reward.length)%10 != 0 ){ this.total_page=this.total_page+1; }
    if( this.reward.length==0 ){ this.total_page=1; }
    this.current_page=1;
    this.total_page=Number(this.total_page.toFixed());
    this.get_page();
  } 

  async get_page(){
    let loading = await this.loadCtrl.create();
    await loading.present(); 
    let temp : any = [];
    let max = this.current_page*10;
    let min = max-10;
    for(let a=min; a<max; a++){
      if(this.reward[a]!=null){ temp.push(this.reward[a]); }
    }
    this.display=temp;
    loading.dismiss();
  } 

  async filter_company(){
    this.startdate = "2019-01-01";
    this.enddate  = ( (new Date().toISOString()).split("T") )[0];
    let loading = await this.loadCtrl.create();
    await loading.present();
    if(this.choose_company!=0){ 
      let temp : any = [];
      for(let a=0; a<this.data.reward.length; a++ ){
         if(this.data.reward[a].compid==this.choose_company){ temp.push(this.data.reward[a])}
      }
      this.reward=temp;
      this.count_page();
    }
    else{ this.reset(); }
    loading.dismiss();
  }

  async get_company(){
    let loading = await this.loadCtrl.create();
    await loading.present(); 
    let temp : any = [];
    for(let a=0; a<this.reward.length; a++){
        let b=0
        for(; b<temp.length; b++){ if(this.reward[a]["compid"]==temp[b]["compid"]){ break; } }
        if(b==temp.length){ temp.push(this.reward[a]); }
    }
    this.company_list=temp;
    loading.dismiss();
  }

  company_name(compid:number){
    for(let a=0; a<this.data.company.length; a++ ){
      if(this.data.company[a].id==compid){ return this.data.company[a]["title"]; }
    }
  }

  async coming() {
    const alert = await this.alertCtrl.create({
      header: 'Stay Tuned!',
      message: 'More features coming soon',
      buttons: ['OK']
    });

    await alert.present();
  }

  

}
