import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.page.html',
  styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {

  constructor( public navCtrl: NavController, public loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {

  }
  
}