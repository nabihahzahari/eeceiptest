import { Component, OnInit } from '@angular/core';
import { GoogleMap, GoogleMaps, GoogleMapsEvent,LatLng,
  CameraPosition,  MarkerOptions,Marker, Environment } 
   from '@ionic-native/google-maps';
import { LoadingController, Platform } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { DataService } from '../../providers/data/data.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {

  map: GoogleMap; 
  message : string ;
  lat : number ;
  log : number ;

  status: string= "not click";

  constructor(private plt: Platform,
              private loadctrl: LoadingController,
              private navCtrl: NavController,
              private data: DataService
              ) {
     this.plt.ready().then( ()=>{
        this.loadMap();
     })
  }

  async ngOnInit() {
  /*  let loading = await this.loadctrl.create();    
    await loading.present();
    await Environment.setEnv({
      API_KEY_FOR_BROWSER_RELEASE: "AIzaSyARPZAm60TGp_OrpSpz2wnxKg-jCtU30DE",
      API_KEY_FOR_BROWSER_DEBUG: "AIzaSyARPZAm60TGp_OrpSpz2wnxKg-jCtU30DE"
    });
    await this.plt.ready().then( ()=> {this.loadMap();});
    loading.dismiss*/
  }

  display : string;
  
  async loadMap(){
    let loading = await this.loadctrl.create();    
    await loading.present();
    this.map = GoogleMaps.create('map_canvas');  
    await this.map.one(GoogleMapsEvent.MAP_READY).then(()=>{   
      var options = { timeout: 20000 }
      /* Geolocation.getCurrentPosition().then(async (resp) => {
          let loading = await this.loadctrl.create();    
          await loading.present();
          let loc : LatLng = new LatLng(resp.coords.latitude,resp.coords.longitude);
          await this.moveCamera(loc); 
          let pos : LatLng = new LatLng(resp.coords.latitude,resp.coords.longitude);
          await this.ownMarker(pos,"Your Location").then((marker:Marker)=>{ marker.showInfoWindow(); })
          this.display = "yes";
          loading.dismiss();
       }).catch(async (error) => { 
          let loading = await this.loadctrl.create();    
          await loading.present();
          let loc : LatLng = new LatLng(3.044677,101.446837);
          this.moveCamera(loc);   
          this.display ="no";
          loading.dismiss();
       });*/

       let loc : LatLng = new LatLng(3.044677,101.446837);
        this.moveCamera(loc);   

      /*   let pos : LatLng = new LatLng(3.086029,101.443148);
        this.createMarker(pos,"Deja Woof Cafe","3").then((marker:Marker)=>{ }) */

        for(let a=0; a<this.data.company.length; a++){ 
          let pos : LatLng = new LatLng(this.data.company[a]["lat"],this.data.company[a]["lng"]);
          this.createMarker(pos,this.data.company[a]["title"]+"  ( "+this.data.company[a]["label"]+" )")
          .then((marker:Marker)=>{ 
            marker.addEventListener(GoogleMapsEvent.INFO_CLICK).subscribe(e => {
                //this.status=this.data[a]["title"];
                 this.navCtrl.navigateForward('receipt');
            });
          })
        }
        //this.locate();
    }, (error)=>{loading.dismiss();} )    
    await loading.dismiss();
  }

  /*async locate(){
      let loading = await this.loadctrl.create();    
      await loading.present();
      this.geolocation.getCurrentPosition().then((resp) => {
        let loc : LatLng = new LatLng(resp.coords.latitude,resp.coords.longitude);
        this.moveCamera(loc); 
        let pos : LatLng = new LatLng(resp.coords.latitude,resp.coords.longitude);
        this.ownMarker(pos,"Your Location").then((marker:Marker)=>{ marker.showInfoWindow(); })
        this.display = "yes";      
      }).catch(async (error) => { 
        let loc : LatLng = new LatLng(3.044677,101.446837);
        this.moveCamera(loc);   
        this.display ="no";
      });
      await loading.dismiss();
  }*/

  moveCamera(loc : LatLng){
    let options : CameraPosition<LatLng> = {target:loc, zoom:11, tilt:10};
    this.map.moveCamera(options);
  } 

  createMarker(loc:LatLng, title:string){
    let markerOptions: MarkerOptions = { position:loc, title:title, 
      icon:'http://labs.google.com/ridefinder/images/mm_20_blue.png',
       };
    return this.map.addMarker(markerOptions);
  } 

  ownMarker(loc:LatLng, title:string){
    let markerOptions: MarkerOptions = { position:loc, title:title, 
      icon:'http://labs.google.com/ridefinder/images/mm_20_red.png',
       };
    return this.map.addMarker(markerOptions);
  } 

  click_window(ttl:string){
    this.status=ttl;
  }


}
