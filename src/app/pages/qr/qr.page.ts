import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { UserProvider } from '../../providers';
import { NavController, Events, AlertController,  LoadingController, ModalController} from '@ionic/angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { RatePage } from '../rate/rate.page';

import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

@Component({
  selector: 'app-qr',
  templateUrl: './qr.page.html',
  styleUrls: ['./qr.page.scss'],
})
export class QrPage implements OnInit {
    dataqr: string = '';
    user: any;
    id:any;
    receipt:any;
    dvc:string;
    q1:number=0;
    imag: any; 
    display_fab:string="false";

  constructor( private codeScanner : BarcodeScanner, 
               private userService : UserProvider,
               public navCtrl : NavController, 
               public events : Events,
               private alertCtrl : AlertController,
               private loadctrl : LoadingController,
               private modalctrl : ModalController ) {
  }

  ngOnInit(){
  }

  async ionViewWillEnter() {
    this.display_fab="false";
    let loading = await this.loadctrl.create();    
    await loading.present();
    await this.getUser();
    await this.codeScanner.scan({
      preferFrontCamera:false, showFlipCameraButton:true, showTorchButton:true, torchOn:false, 
      prompt:'Eeceipt Scanner', resultDisplayDuration:0, disableAnimations:true, disableSuccessBeep:false
    }).then( result => {
        if(result['text'] != ''){
          this.receipt = [] ;
          this.dataqr = result['text'].substr(42);
          this.getScanData(this.dataqr, this.id);
        }else{ alert('Input Error'); this.back(); }
      }).catch( err =>{ 
         alert('Sorry, there was an error with scanner.'); this.back();
         //this.getScanData('eb0f18cf-83a3-402a-a9c3-90592c12790e','5d2f1eb133c38872e438e17e');
      });
    await loading.dismiss();
    //this.getScanData('5d35523533c3887495416f28', this.id);
    //this.displayrate();
  }

  async ionViewDidEnter(){
    let loading = await this.loadctrl.create();
    await loading.present();
    var data = document.getElementById('rec'); 
    await html2canvas(data).then(canvas => {   
      var contentDataURL = canvas.toDataURL('image/png'); 
      this.imag = "base64:receipt.png//" + contentDataURL.substr(22);
    });   
    await loading.dismiss().then( ()=>{this.display_fab="true";} );
  }  

  getUser(){
    this.userService.getUserInfo().then((response:any) => {
       this.user = response;
       this.id = response._id;
    }).catch(err => {this.navCtrl.navigateRoot('authentication'); })
  }

  display:string ;

  async getScanData (dataqr, id){
    let loadi = await this.loadctrl.create();    
    await loadi.present();
    await this.userService.getReceipt(id).then( async (response: any) => {
       for(let a=0; a<response.length; a++){
         if(response[a]["receiptId"]==dataqr){ 
           this.receipt=response[a]; 
           alert("You have claimed this eeceipt");            
           //alert(response[a]["_id"]); this.displayrate();
         }
       }
    }).catch(err => { alert("error checking"); this.back();})    
    if(this.receipt==""){
       this.userService.getScanData(dataqr, id).then((response: any) => {
           if(response["error"]!=null){ alert("eeceipt has been claimed by other user"); this.back(); }
           else{ this.receipt = response; this.displayrate(); }
       }).catch(err => { alert("Not valid"); this.back();})
    }
    await loadi.dismiss();
  }

  back(){ this.navCtrl.navigateForward('home'); }
  error(){ alert("receipt error"); this.back(); }
  coming(){ alert("more features are coming soon"); }

  async email(){
    let loading = await this.loadctrl.create();
    await loading.present(); 
    let email = {
      //app: 'gmail',
      to: '', cc: '', bcc:'', attachments: [this.imag],
      subject: 'My Eeceipt Copy',
      body: '<br>Thank you for using Eeceipt.<br>Please download our <a href="linked">mobile app</a> to save your receipts and  enjoy future discounts & rewards from our partners',
      isHtml: true
    }
    await EmailComposer.open(email).then(()=>{  }).catch(err=>{ alert("No email app found"); })
    loading.dismiss();
  }

 /* async displayrate2( ){ 
    let loading = await this.loadctrl.create();
    await loading.present();     
    await loading.dismiss();const modal = await this.modalctrl.create({ component: RatePage, cssClass:"rate-modal"});
    return await modal.present(); 
 }  */

 async displayrate( ){ 
  let loading = await this.loadctrl.create();
  await loading.present();     
  await loading.dismiss();
  const modal = await this.modalctrl.create({ 
    component: RatePage,
    cssClass:"rate-modal",
    componentProps: {
      'q1':0, 'q2':0, 'q3':0, 'comment':'',
      'receipt_id':this.receipt["_id"]
      //'receipt_id':'5d35523533c3887495416f28'
      //star:this.star
    }
  });    
  modal.onDidDismiss().then((data) => {
    if (data["data"] != null){ this.q1=data["data"]["q1"]; }
  });
  return await modal.present(); 
} 

}

