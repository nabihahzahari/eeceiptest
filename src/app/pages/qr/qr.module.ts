import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PopupmenuPageModule } from '../popupmenu/popupmenu.module';

import { IonicModule } from '@ionic/angular';

import { QrPage } from './qr.page';
import { PinchZoomModule } from 'ngx-pinch-zoom';
import { PipesModule } from '../../../pipes/pipe.module';
// import { PipesModule } from './src/app/pipes/pipe.module';



const routes: Routes = [
  {
    path: '',
    component: QrPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PinchZoomModule,
    PopupmenuPageModule,
    PipesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [QrPage]
})
export class QrPageModule {}
