import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RdetailPage } from './rdetail.page';
import { PipesModule } from '../../../pipes/pipe.module';
import { PinchZoomModule } from 'ngx-pinch-zoom';



const routes: Routes = [
  {
    path: '',
    component: RdetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    PinchZoomModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RdetailPage]
})
export class RdetailPageModule {}
