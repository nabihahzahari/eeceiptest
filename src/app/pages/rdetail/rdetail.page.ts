import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, NavParams, ModalController, LoadingController, NavController } from '@ionic/angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { ApiService } from '../../api.service';
import { RatePage } from '../rate/rate.page';
import {DomSanitizer, SafeResourceUrl, SafeHtml, SafeUrl, SafeStyle} from '@angular/platform-browser'
import * as jspdf from 'jspdf';
import * as html2canvas from 'html2canvas';

@Component({
  selector: 'app-rdetail',
  templateUrl: './rdetail.page.html',
  styleUrls: ['./rdetail.page.scss'],
})
export class RdetailPage implements OnInit {

    modalTitle:string;
    modelId:SafeHtml;
    q1:number; q2:number; q3:number; comment:string;
    receiptdetail: any;
    star:number=4;
    display : string ="koko";     
    //pdf: any = new jspdf('p', 'mm', 'a4');
    imag: any; 
    //imag:any;
    receipt_id:any;
    display_fab:string="false";

  constructor( private modalController: ModalController, 
               public router: Router, 
               public navParams: NavParams,
               //private emailComposer: EmailComposer,
               private loadCtrl: LoadingController,
               private apiservice: ApiService,
               private platform: Platform,
               private sanitizer: DomSanitizer,
               private navctrl: NavController) {
  }

  now(){
    //this.apiservice.getdata("test1").subscribe( (res)=>{this.display=res;}, (err)=>{this.display="takdapat";} )
    this.apiservice.postdata("test2",{}).subscribe(res=>{this.display=res;}, err=>{this.display="takdapat";} )
  }

  ngOnInit(){}
  
  displa:string ="mula";
  ionViewWillEnter() {
    this.display_fab="false";
    // console.table(this.navParams);
    this.modelId = this.sanitizer.bypassSecurityTrustHtml(this.navParams.data.html);
    this.modalTitle = this.navParams.data.receiptId;
    this.q1 = this.navParams.data.q1;
    this.q2 = this.navParams.data.q2;
    this.q3 = this.navParams.data.q3;
    this.comment = this.navParams.data.comment;
    this.receipt_id = this.navParams.data._id;
    if(this.navParams.data.star==null){ this.displa="null"; //this.displayrate(); 
    }
    else{ this.displa="taknull"; }
  }

  //imagi:any;
  async ionViewDidEnter(){
    let loading = await this.loadCtrl.create();
    await loading.present();
    var data = document.getElementById('rec'); 
    await html2canvas(data).then(canvas => {   
      var contentDataURL = canvas.toDataURL('image/png'); 
      this.imag = "base64:receipt.png//" + contentDataURL.substr(22);
      //this.imagi = contentDataURL;
    });   
    await loading.dismiss().then( (result)=>{this.display_fab="true";} );
  }

  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss();
  }

  //back(){ this.navctrl.navigateForward('receipt'); }
  coming(){ alert("more features are coming soon"); }   

  async email(){
    let loading = await this.loadCtrl.create();
    await loading.present();    
         let email = {
            //app: 'gmail',
            to: '', cc: '', bcc:'', attachments: [this.imag],
            subject: 'My Eeceipt Copy',
            body: '<br>Thank you for using Eeceipt.<br>Please download our <a href="linked">mobile app</a> to save your receipts and  enjoy future discounts & rewards from our partners',
            isHtml: true
         }
         await EmailComposer.open(email).then(()=>{  }).catch(err=>{ alert("No email app found"); })    
    await loading.dismiss();
  }

  async displayrate( ){ 
    let loading = await this.loadCtrl.create();
    await loading.present();     
    await loading.dismiss();
    const modal = await this.modalController.create({ 
      component: RatePage,
      cssClass:"rate-modal",
      componentProps: {
        'q1':this.q1, 'q2':this.q2, 'q3':this.q3, 'comment':this.comment,
        'receipt_id' : this.receipt_id
      }
    });    
    modal.onDidDismiss().then((data) => {
      if (data["data"] != null){ 
        this.q1=data["data"]["q1"];
        /*this.q2=data["data"]["q2"];
        this.q3=data["data"]["q3"];
        this.comment=data["data"]["comment"];*/
      }
    });
    return await modal.present(); 
  }    

  dato : any ;  
  datas : string = "jur"

}
