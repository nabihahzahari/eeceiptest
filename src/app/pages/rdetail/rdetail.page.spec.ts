import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RdetailPage } from './rdetail.page';

describe('RdetailPage', () => {
  let component: RdetailPage;
  let fixture: ComponentFixture<RdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RdetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
