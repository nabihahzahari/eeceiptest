import { Injectable } from '@angular/core';
import { LoadingController} from '@ionic/angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: Http,
              private loadctrl: LoadingController) { }

  postdata(api: string, data: any){
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post("http://localhost:8080/api/" + api, data, options)
    .map(res => res.json());
  }

  getdata(api: string){
    return this.http.get("http://localhost:8080/api/" + api)
    .map(res => res.json());
  }  

}
