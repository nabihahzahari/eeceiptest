export interface Pages {
    title: string;
    url: any;
    direct?: string;
    icon?: string;
    content?: string;
    onclick?: string;
}
