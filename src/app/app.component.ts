import { Component } from '@angular/core';

import { Platform, NavController, AlertController, Events, MenuController, LoadingController, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { TranslateProvider } from './providers/translate/translate.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';

import { Pages } from './interfaces/pages';
import { AuthProvider, UserProvider } from './providers';
import { HomePage } from '../app/pages/home/home.page';
import { Storage } from '@ionic/storage';
import { AccountmodalPage } from './pages/accountmodal/accountmodal.page';
//import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

/**
 * Main Wrap App Component with starting methods
 *
 * @export
 * @class AppComponent
 */
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
    user: any;

  /**
   * Creates an Array instance with <Pages> interface that receives all menu list.
   *
   * @type {Array<Pages>}
   * @memberof AppComponent
   */
  public appPages: Array<Pages>;
  /**
   * Creates an instance of AppComponent.
   * @param {Platform} platform
   * @param {SplashScreen} splashScreen
   * @param {StatusBar} statusBar
   * @param {TranslateProvider} translate
   * @param {TranslateService} translateService
   * @param {NavController} navCtrl
   * @memberof AppComponent
   */
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateProvider,
    private translateService: TranslateService,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private authService: AuthProvider,
    public events: Events,
    private userService: UserProvider,
    private storage: Storage,
    private menuctrl: MenuController,
    private loadctrl: LoadingController,
    private modalController: ModalController
    //private fb: Facebook
  ) {
    this.events.subscribe('user', (response) => {
        // user and time are the same arguments passed in `events.publish(user, time)`
        // this.user.id = data.id;
        // console.log(response);
        //  console.log('Welcome', response.name);
         this.user = response;
        //  console.log(response);
      });
    
    this.appPages = [
      { title: 'Home', url: '/home', direct: 'root', icon: 'home' },
      { title: 'Receipt', url: '/receipt', direct: 'forward', icon: 'paper' },
      { title: 'Analytic', url: '/charts', direct: 'forward', icon: 'pie' },
      { title: 'Reward', url: '/reward', direct: 'forward', icon: 'medal' },
      { title: 'Location', url: '/location', direct: 'forward', icon: 'pin'  },
    // { title: 'Wallet', url: '/wallet', direct: 'forward', icon: 'wallet' },
      { title: 'About', url: '/about', direct: 'forward', icon: 'information-circle-outline' },
      { title: 'Support', url: '/support', direct: 'forward', icon: 'settings' }    
    ];

    this.initializeApp();
  }
/**
 * Method that starts all Cordova and Factories
 *
 * @memberof AppComponent
 */
initializeApp() {
    //this.storage.set("total",1);
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 1000);
      // Set language of the app.
      this.translateService.setDefaultLang(environment.language);
      this.translateService.use(environment.language);
      this.translateService.getTranslation(environment.language).subscribe(translations => {
        this.translate.setTranslations(translations);
      });
    }).catch(() => {
      // Set language of the app.
      this.translateService.setDefaultLang(environment.language);
      this.translateService.use(environment.language);
      this.translateService.getTranslation(environment.language).subscribe(translations => {
        this.translate.setTranslations(translations);
      });
    });
  }
  /**
   * Navigate to Edit Profile Page
   *
   * @memberof AppComponent
   */
  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }
/**
 * Logout Method
 *
 * @memberof AppComponent
 */
logout () {
    this.authService.removeCredentials();
    setTimeout(() => {
        // this.menuCtrl.enable(true);
        this.navCtrl.navigateForward('/authentication');
        //this.navCtrl.navigateForward('walkthrough');
    }, 750)
    //this.fb.logEvent('');
    //this.fb.logout();
  }

  async coming() {
    const alert = await this.alertCtrl.create({
      header: 'Stay Tuned!',
    //   subHeader: 'Subtitle',
      message: 'More features coming soon',
      buttons: ['OK']
    });

    await alert.present();
  }

  closemeinu(){ this.menuctrl.close(); }

  async openaccount(account:string){
    const modal = await this.modalController.create({
      component: AccountmodalPage,
      componentProps: {'account' : account},
      cssClass: 'account-modal'
    });
    return await modal.present();
  }

/*userdata:any;
async fbaccount(){
  let loading = await this.loadctrl.create();
  await loading.present(); 
  await this.storage.get('fbuser').then((val) => {
    alert(JSON.stringify(val));
    if(val==null){
      this.fb.login(['public_profile', 'user_friends', 'email']).then(async (res: FacebookLoginResponse) =>{
       //alert(JSON.stringify(email))
        await this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then( async profile => {
          this.userdata = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']};
          await this.storage.set('fbuser',this.userdata);
          //let data = { email: profile['email'], password:'' }
        }).catch(e => alert("Backend Error") );   
      }).catch(e => alert("Error Login to Facebook") );    
    }
    else{ alert("Logged In"); }
  });
  //await this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);
  await this.loadctrl.dismiss();
}*/

}

