import { NgModule } from '@angular/core';
import { HtmlPipe } from './html.pipe';

@NgModule({
  declarations: [
    HtmlPipe,
  ],
  imports: [

  ],
  exports: [
    HtmlPipe
  ]
})
export class PipesModule { }