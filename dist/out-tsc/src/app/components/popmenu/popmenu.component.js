import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
var PopmenuComponent = /** @class */ (function () {
    function PopmenuComponent(navCtrl) {
        this.navCtrl = navCtrl;
        this.openMenu = false;
    }
    PopmenuComponent.prototype.ngOnInit = function () {
    };
    PopmenuComponent.prototype.togglePopupMenu = function () {
        return this.openMenu = !this.openMenu;
    };
    PopmenuComponent.prototype.togglePopupClose = function () {
        return this.openMenu = false;
    };
    // // //
    PopmenuComponent.prototype.receipt = function () {
        this.navCtrl.navigateForward('receipt');
        this.togglePopupClose();
    };
    PopmenuComponent.prototype.scanQr = function () {
        // this.navCtrl.navigateRoot('charts');
        this.navCtrl.navigateForward('qr');
        this.togglePopupClose();
    };
    PopmenuComponent.prototype.analytic = function () {
        // this.navCtrl.navigateRoot('charts');
        this.navCtrl.navigateForward('charts');
        this.togglePopupClose();
    };
    PopmenuComponent.prototype.wallet = function () {
        // this.navCtrl.navigateRoot('charts');
        this.navCtrl.navigateForward('wallet');
        this.togglePopupClose();
    };
    PopmenuComponent.prototype.home = function () {
        this.navCtrl.navigateForward('home');
        this.togglePopupClose();
    };
    PopmenuComponent = tslib_1.__decorate([
        Component({
            selector: 'popmenu',
            templateUrl: './popmenu.component.html',
            styleUrls: ['./popmenu.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NavController])
    ], PopmenuComponent);
    return PopmenuComponent;
}());
export { PopmenuComponent };
//# sourceMappingURL=popmenu.component.js.map