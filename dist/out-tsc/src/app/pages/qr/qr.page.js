import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { UserProvider } from '../../providers';
import { NavController, Events, AlertController, LoadingController, ModalController } from '@ionic/angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { RatePage } from '../rate/rate.page';
import * as html2canvas from 'html2canvas';
var QrPage = /** @class */ (function () {
    function QrPage(codeScanner, userService, navCtrl, events, alertCtrl, loadctrl, modalctrl) {
        this.codeScanner = codeScanner;
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.loadctrl = loadctrl;
        this.modalctrl = modalctrl;
        this.dataqr = '';
        this.star = 0;
    }
    QrPage.prototype.ngOnInit = function () {
    };
    QrPage.prototype.ionViewWillEnter = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadctrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.getUser()];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.codeScanner.scan({
                                preferFrontCamera: false, showFlipCameraButton: true, showTorchButton: true, torchOn: false,
                                prompt: 'Eeceipt Scanner', resultDisplayDuration: 0, disableAnimations: true, disableSuccessBeep: false
                            }).then(function (result) {
                                if (result['text'] != '') {
                                    _this.receipt = [];
                                    _this.dataqr = result['text'].substr(42);
                                    _this.getScanData(_this.dataqr, _this.id);
                                }
                                else {
                                    alert('Input Error');
                                    _this.back();
                                }
                            }).catch(function (err) { alert('Sorry, there was an error with scanner.'); _this.back(); })];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, loading.dismiss()];
                    case 5:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    QrPage.prototype.ionViewDidEnter = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, data;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadctrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        data = document.getElementById('rec');
                        return [4 /*yield*/, html2canvas(data).then(function (canvas) {
                                var contentDataURL = canvas.toDataURL('image/png');
                                _this.imag = "base64:receipt.png//" + contentDataURL.substr(22);
                            })];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, loading.dismiss()];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    QrPage.prototype.getUser = function () {
        var _this = this;
        this.userService.getUserInfo().then(function (response) {
            _this.user = response;
            _this.id = response._id;
        }).catch(function (err) { _this.navCtrl.navigateRoot('authentication'); });
    };
    QrPage.prototype.getScanData = function (dataqr, id) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loadi;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadctrl.create()];
                    case 1:
                        loadi = _a.sent();
                        return [4 /*yield*/, loadi.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.userService.getReceipt(id).then(function (response) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                var a;
                                return tslib_1.__generator(this, function (_a) {
                                    for (a = 0; a < response.length; a++) {
                                        if (response[a]["receiptId"] == dataqr) {
                                            this.receipt = response[a];
                                            alert("You have claimed this eeceipt");
                                        }
                                    }
                                    return [2 /*return*/];
                                });
                            }); }).catch(function (err) { alert("error checking"); _this.back(); })];
                    case 3:
                        _a.sent();
                        if (this.receipt == "") {
                            this.userService.getScanData(dataqr, id).then(function (response) {
                                if (response["error"] != null) {
                                    alert("eeceipt has been claimed by other user");
                                    _this.back();
                                }
                                else {
                                    _this.receipt = response;
                                    _this.displayrate();
                                }
                            }).catch(function (err) { alert("Not valid"); _this.back(); });
                        }
                        return [4 /*yield*/, loadi.dismiss()];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    QrPage.prototype.back = function () { this.navCtrl.navigateForward('home'); };
    QrPage.prototype.error = function () { alert("receipt error"); this.back(); };
    QrPage.prototype.coming = function () { alert("more features are coming soon"); };
    QrPage.prototype.email = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, email;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadctrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        email = {
                            app: 'gmail',
                            to: '', cc: '', bcc: '', attachments: [this.imag],
                            subject: 'My Eeceipt Copy',
                            body: '<br>Thank you for using Eeceipt.<br>Please download our <a href="linked">mobile app</a> to save your receipts and  enjoy future discounts & rewards from our partners',
                            isHtml: true
                        };
                        return [4 /*yield*/, EmailComposer.open(email).then(function () { }).catch(function (err) { alert("No email app found"); })];
                    case 3:
                        _a.sent();
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    /* async displayrate2( ){
       let loading = await this.loadctrl.create();
       await loading.present();
       await loading.dismiss();const modal = await this.modalctrl.create({ component: RatePage, cssClass:"rate-modal"});
       return await modal.present();
    }  */
    QrPage.prototype.displayrate = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, modal;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadctrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, loading.dismiss()];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.modalctrl.create({
                                component: RatePage,
                                cssClass: "rate-modal",
                                componentProps: {
                                    'star': this.star
                                }
                            })];
                    case 4:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (data) {
                            if (data["data"] != null) {
                                _this.star = data["data"];
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 5: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    QrPage = tslib_1.__decorate([
        Component({
            selector: 'app-qr',
            templateUrl: './qr.page.html',
            styleUrls: ['./qr.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [BarcodeScanner,
            UserProvider,
            NavController,
            Events,
            AlertController,
            LoadingController,
            ModalController])
    ], QrPage);
    return QrPage;
}());
export { QrPage };
//# sourceMappingURL=qr.page.js.map