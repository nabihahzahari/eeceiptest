import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PopupmenuPageModule } from '../popupmenu/popupmenu.module';
import { IonicModule } from '@ionic/angular';
import { QrPage } from './qr.page';
import { PinchZoomModule } from 'ngx-pinch-zoom';
import { PipesModule } from '../../../pipes/pipe.module';
// import { PipesModule } from './src/app/pipes/pipe.module';
var routes = [
    {
        path: '',
        component: QrPage
    }
];
var QrPageModule = /** @class */ (function () {
    function QrPageModule() {
    }
    QrPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                PinchZoomModule,
                PopupmenuPageModule,
                PipesModule,
                RouterModule.forChild(routes)
            ],
            declarations: [QrPage]
        })
    ], QrPageModule);
    return QrPageModule;
}());
export { QrPageModule };
//# sourceMappingURL=qr.module.js.map