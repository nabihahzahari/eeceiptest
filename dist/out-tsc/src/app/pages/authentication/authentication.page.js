import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { TranslateProvider, AuthProvider } from '../../providers';
import { decodeLaravelErrors } from '../../functions/Helpers';
var AuthenticationPage = /** @class */ (function () {
    function AuthenticationPage(navCtrl, menuCtrl, toastCtrl, alertCtrl, loadingCtrl, translate, formBuilder, authService) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.translate = translate;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.authSegment = 'login';
        this.message = "kosong jek";
    }
    AuthenticationPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    AuthenticationPage.prototype.ngOnInit = function () {
        this.onLoginForm = this.formBuilder.group({
            'email': [null, Validators.compose([Validators.required])],
            'password': [null, Validators.compose([Validators.required])]
        });
        this.onRegisterForm = this.formBuilder.group({
            'firstName': [null, Validators.compose([Validators.required])],
            'lastName': [null, Validators.compose([Validators.required])],
            'email': [null, Validators.compose([Validators.maxLength(70), Validators.email, Validators.required])],
            'password': [null, Validators.compose([Validators.required])],
            'password_confirmation': [null, Validators.compose([Validators.required])]
        });
    };
    AuthenticationPage.prototype.checkAuthenticated = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isAuthenticated, err_1, alert_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 5]);
                        return [4 /*yield*/, this.authService.checkIsAuthenticated()];
                    case 1:
                        isAuthenticated = _a.sent();
                        if (isAuthenticated) {
                            this.menuCtrl.enable(true);
                            this.navCtrl.navigateForward('/home');
                        }
                        return [3 /*break*/, 5];
                    case 2:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [4 /*yield*/, this.alertCtrl.create({ header: 'Error', message: 'Error on verify authentication info', buttons: ['Ok'] })];
                    case 3:
                        alert_1 = _a.sent();
                        return [4 /*yield*/, alert_1.present()];
                    case 4:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    AuthenticationPage.prototype.coming = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Stay Tuned!',
                            message: 'More features coming soon',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthenticationPage.prototype.loadingCircle = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({ message: 'Please wait...' })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthenticationPage.prototype.doLogin = function (rawLogin) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var data, a, alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = JSON.stringify(rawLogin);
                        a = JSON.parse(data);
                        return [4 /*yield*/, this.alertCtrl.create({ message: 'Logging in, please wait...' })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        this.authService.login(a).then(function (response) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, alert.dismiss()];
                                    case 1:
                                        _a.sent();
                                        this.authService.storeCredentials(response);
                                        setTimeout(function () { _this.checkAuthenticated(); }, 750);
                                        return [2 /*return*/];
                                }
                            });
                        }); })
                            .catch(function (err) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var toast, toasti, toaste, toasturrr;
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, alert.dismiss()];
                                    case 1:
                                        _a.sent();
                                        return [4 /*yield*/, this.toastCtrl.create({
                                                message: '(Error Code:400) We are sorry, but it appears that there has been an internal server error. Our engineer have been notified and working to resolve the issue.',
                                                position: 'middle',
                                                duration: 5000,
                                                color: 'danger'
                                            })];
                                    case 2:
                                        toast = _a.sent();
                                        return [4 /*yield*/, alert.dismiss()];
                                    case 3:
                                        _a.sent();
                                        return [4 /*yield*/, this.toastCtrl.create({
                                                message: 'Unconfirmed / Incorrect e-Mail or Password, please try again.',
                                                showCloseButton: false,
                                                // closeButtonText: 'hide',
                                                position: 'middle',
                                                duration: 3000,
                                                color: 'danger'
                                            })];
                                    case 4:
                                        toasti = _a.sent();
                                        return [4 /*yield*/, alert.dismiss()];
                                    case 5:
                                        _a.sent();
                                        return [4 /*yield*/, this.toastCtrl.create({
                                                message: '(Error Code:500) We are sorry, but it appears that there has been an internal server error. Our engineer have been notified and working to resolve the issue.',
                                                showCloseButton: false,
                                                // closeButtonText: 'hide',
                                                position: 'middle',
                                                duration: 5000,
                                                color: 'danger'
                                            })];
                                    case 6:
                                        toaste = _a.sent();
                                        return [4 /*yield*/, this.toastCtrl.create({
                                                message: 'weiweiewei',
                                                showCloseButton: false,
                                                // closeButtonText: 'hide',
                                                position: 'middle',
                                                duration: 5000,
                                                color: 'danger'
                                            })];
                                    case 7:
                                        toasturrr = _a.sent();
                                        if (err.status === 400) {
                                            toast.present();
                                        }
                                        if (err.status === 401) {
                                            toasti.present();
                                        }
                                        if (err.status === 500) {
                                            toaste.present();
                                        }
                                        if (err.status === 503) {
                                            toasturrr.present();
                                        }
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthenticationPage.prototype.doRegister = function (rawReg) {
        var _this = this;
        // let data = JSON.stringify(rawReg);
        // let a = JSON.parse(data);
        this.authService.register(rawReg)
            .then(function (response) {
            _this.navCtrl.navigateForward('econfirm');
        })
            .catch(function (err) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var decodedErrors, toasta;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        decodedErrors = decodeLaravelErrors(err);
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: decodedErrors.errors.join(' | '),
                                showCloseButton: false,
                                position: 'middle',
                                duration: 6000,
                                color: 'danger'
                            })];
                    case 1:
                        toasta = _a.sent();
                        if (err.status === 422) {
                            toasta.present();
                        }
                        return [2 /*return*/];
                }
            });
        }); });
    };
    /*  doRegister (rawReg){
         this.message = "masuk function saja";
         this.authService.register(rawReg)
        .then( (response:any) => {
            //this.navCtrl.navigateForward('econfirm');
            this.message = "patutnya buka nie"
         })/*
        .catch(async (err:any) => {
            let decodedErrors: any = decodeLaravelErrors(err);
            const toasta = await this.toastCtrl.create({
                  message: "xleh buka lah",
                  showCloseButton: false,
                  position: 'middle',
                  duration: 6000,
                  color: 'danger'
            });
           // if (err.status === 422) { toasta.present(); }
           toasta.present();
        });
   }*/
    AuthenticationPage.prototype.validateLoginData = function (data) {
        return true;
    };
    AuthenticationPage = tslib_1.__decorate([
        Component({
            selector: 'app-authentication',
            templateUrl: './authentication.page.html',
            styleUrls: ['./authentication.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            ToastController,
            AlertController,
            LoadingController,
            TranslateProvider,
            FormBuilder,
            AuthProvider])
    ], AuthenticationPage);
    return AuthenticationPage;
}());
export { AuthenticationPage };
//# sourceMappingURL=authentication.page.js.map