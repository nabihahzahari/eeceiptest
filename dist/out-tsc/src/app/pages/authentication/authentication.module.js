import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { AuthenticationPage } from './authentication.page';
var routes = [
    {
        path: '',
        component: AuthenticationPage
    }
];
var AuthenticationPageModule = /** @class */ (function () {
    function AuthenticationPageModule() {
    }
    AuthenticationPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                TranslateModule.forChild()
            ],
            declarations: [AuthenticationPage]
        })
    ], AuthenticationPageModule);
    return AuthenticationPageModule;
}());
export { AuthenticationPageModule };
//# sourceMappingURL=authentication.module.js.map