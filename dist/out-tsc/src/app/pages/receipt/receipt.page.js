import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UserProvider } from '../../providers';
import { NavController, AlertController, ModalController, LoadingController } from '@ionic/angular';
import { RdetailPage } from '../rdetail/rdetail.page';
import { DataService } from '../../providers/data/data.service';
var ReceiptPage = /** @class */ (function () {
    function ReceiptPage(userService, navCtrl, alertCtrl, modalController, loadCtrl, data) {
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.modalController = modalController;
        this.loadCtrl = loadCtrl;
        this.data = data;
        this.receipt = [];
        this.sum = 0;
        this.startdate = "2019-01-01";
        this.enddate = ((new Date().toISOString()).split("T"))[0];
        this.current_page = 1;
        this.display = [];
        this.company_list = [];
        this.choose_company = "0";
        this.star = 3;
    }
    ReceiptPage.prototype.ngOnInit = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () { return tslib_1.__generator(this, function (_a) {
            return [2 /*return*/];
        }); });
    };
    ReceiptPage.prototype.next = function () {
        this.current_page = this.current_page + 1;
        this.get_page();
    };
    ReceiptPage.prototype.previous = function () {
        this.current_page = this.current_page - 1;
        this.get_page();
    };
    ReceiptPage.prototype.ionViewWillEnter = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.getUser()];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, loading.dismiss()];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReceiptPage.prototype.getUser = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.userService.getUserInfo().then(function (response) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var loading;
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                                    case 1:
                                        loading = _a.sent();
                                        return [4 /*yield*/, loading.present()];
                                    case 2:
                                        _a.sent();
                                        this.user = response;
                                        this.id = response._id;
                                        return [4 /*yield*/, this.getOne(this.id)];
                                    case 3:
                                        _a.sent();
                                        return [4 /*yield*/, loading.dismiss()];
                                    case 4:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); }).catch(function (err) { _this.navCtrl.navigateRoot('authentication'); })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReceiptPage.prototype.getOne = function (id) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.userService.getReceipt(id).then(function (response) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            return tslib_1.__generator(this, function (_a) {
                                this.receipt = response;
                                this.display = response;
                                this.count_page();
                                this.get_company();
                                return [2 /*return*/];
                            });
                        }); }).catch(function (err) { var alert = _this.alertCtrl.create({ header: 'Error', message: 'Error searching', buttons: ['Ok'] }); })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReceiptPage.prototype.getDetail = function (receiptdetail, html, star) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: RdetailPage,
                            componentProps: {
                                'receiptId': receiptdetail,
                                'html': html,
                                'star': star
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (dataReturned) {
                            if (dataReturned !== null) {
                                // console.log('Modal Sent Data :', dataReturned);
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ReceiptPage.prototype.format_startdate = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.startdate = (this.startdate.split("T"))[0];
                        return [4 /*yield*/, this.compare_date()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReceiptPage.prototype.format_enddate = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.enddate = (this.enddate.split("T"))[0];
                        return [4 /*yield*/, this.compare_date()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReceiptPage.prototype.reset = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.display = this.receipt;
                        this.choose_company = "0";
                        this.startdate = "2019-01-01";
                        this.enddate = ((new Date().toISOString()).split("T"))[0];
                        return [4 /*yield*/, this.count_page()];
                    case 3:
                        _a.sent();
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReceiptPage.prototype.compare_date = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, temp, a;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        if (!(this.enddate < this.startdate)) return [3 /*break*/, 3];
                        this.data.presentToast("End date must greater than start date");
                        this.reset();
                        return [3 /*break*/, 9];
                    case 3:
                        temp = [];
                        a = 0;
                        _a.label = 4;
                    case 4:
                        if (!(a < this.receipt.length)) return [3 /*break*/, 7];
                        if (!(((this.receipt[a].date).substr(0, 19) >= this.startdate) &&
                            ((this.receipt[a].date).substr(0, 19) <= this.enddate))) return [3 /*break*/, 6];
                        return [4 /*yield*/, temp.push(this.receipt[a])];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6:
                        a++;
                        return [3 /*break*/, 4];
                    case 7:
                        this.display = temp;
                        return [4 /*yield*/, this.count_page()];
                    case 8:
                        _a.sent();
                        _a.label = 9;
                    case 9: return [4 /*yield*/, loading.dismiss()];
                    case 10:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReceiptPage.prototype.count_page = function () {
        this.total_page = (this.display.length) / 10;
        if ((this.display.length) % 10 != 0) {
            this.total_page = this.total_page + 1;
        }
        this.current_page = 1;
        this.total_page = Number(this.total_page.toFixed());
        this.get_page();
    };
    ReceiptPage.prototype.get_page = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, temp, max, min, a;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        temp = [];
                        max = this.current_page * 10;
                        min = max - 10;
                        for (a = min; a < max; a++) {
                            if (this.display[a] != null) {
                                temp.push(this.display[a]);
                            }
                        }
                        this.display = temp;
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReceiptPage.prototype.get_company = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, temp, a, b;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        temp = [];
                        for (a = 0; a < this.receipt.length; a++) {
                            b = 0;
                            for (; b < temp.length; b++) {
                                if (this.receipt[a]["merchantId"] == temp[b]["merchantId"]) {
                                    break;
                                }
                            }
                            if (b == temp.length) {
                                temp.push(this.receipt[a]);
                            }
                        }
                        this.company_list = temp;
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReceiptPage.prototype.filter_company = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, temp, a;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.startdate = "2019-01-01";
                        this.enddate = ((new Date().toISOString()).split("T"))[0];
                        return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        if (!(this.choose_company != "0")) return [3 /*break*/, 4];
                        temp = [];
                        for (a = 0; a < this.receipt.length; a++) {
                            if (this.receipt[a]["merchantId"] == this.choose_company) {
                                temp.push(this.receipt[a]);
                            }
                        }
                        this.display = temp;
                        return [4 /*yield*/, this.count_page()];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        this.reset();
                        _a.label = 5;
                    case 5: return [4 /*yield*/, loading.dismiss()];
                    case 6:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReceiptPage = tslib_1.__decorate([
        Component({
            selector: 'app-receipt',
            templateUrl: './receipt.page.html',
            styleUrls: ['./receipt.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [UserProvider, NavController, AlertController,
            ModalController, LoadingController, DataService])
    ], ReceiptPage);
    return ReceiptPage;
}());
export { ReceiptPage };
//# sourceMappingURL=receipt.page.js.map