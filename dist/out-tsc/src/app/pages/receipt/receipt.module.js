import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ReceiptPage } from './receipt.page';
import { PopupmenuPageModule } from '../popupmenu/popupmenu.module';
var routes = [
    {
        path: '',
        component: ReceiptPage
    }
];
var ReceiptPageModule = /** @class */ (function () {
    function ReceiptPageModule() {
    }
    ReceiptPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                PopupmenuPageModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ReceiptPage]
        })
    ], ReceiptPageModule);
    return ReceiptPageModule;
}());
export { ReceiptPageModule };
//# sourceMappingURL=receipt.module.js.map