import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { PopmenuComponent } from './../../components/popmenu/popmenu.component';
import { PopupmenuPage } from './popupmenu.page';
var routes = [
    {
        path: '',
        component: PopupmenuPage
    }
];
var PopupmenuPageModule = /** @class */ (function () {
    function PopupmenuPageModule() {
    }
    PopupmenuPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                // RouterModule.forChild(routes),
                TranslateModule.forChild()
            ],
            declarations: [PopupmenuPage, PopmenuComponent],
            exports: [PopmenuComponent]
        })
    ], PopupmenuPageModule);
    return PopupmenuPageModule;
}());
export { PopupmenuPageModule };
//# sourceMappingURL=popupmenu.module.js.map