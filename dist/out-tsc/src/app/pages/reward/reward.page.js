import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { DataService } from '../../providers/data/data.service';
var RewardPage = /** @class */ (function () {
    function RewardPage(alertCtrl, loadCtrl, data) {
        this.alertCtrl = alertCtrl;
        this.loadCtrl = loadCtrl;
        this.data = data;
        this.startdate = "2019-01-01";
        this.enddate = ((new Date().toISOString()).split("T"))[0];
        this.current_page = 1;
        this.reward = this.data.reward;
        this.display = [];
        this.company_list = [];
        this.choose_company = 0;
    }
    RewardPage.prototype.ngOnInit = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.count_page()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.get_company()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RewardPage.prototype.next = function () {
        this.current_page = this.current_page + 1;
        this.get_page();
    };
    RewardPage.prototype.previous = function () {
        this.current_page = this.current_page - 1;
        this.get_page();
    };
    RewardPage.prototype.reset = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.reward = this.data.reward;
                        this.choose_company = 0;
                        this.startdate = "2019-01-01";
                        this.enddate = ((new Date().toISOString()).split("T"))[0];
                        return [4 /*yield*/, this.count_page()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RewardPage.prototype.format_startdate = function () {
        this.startdate = (this.startdate.split("T"))[0];
        this.compare_date();
    };
    RewardPage.prototype.format_enddate = function () {
        this.enddate = (this.enddate.split("T"))[0];
        this.compare_date();
    };
    RewardPage.prototype.compare_date = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, temp, a;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.reward = this.data.reward;
                        return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        if (this.enddate < this.startdate) {
                            this.data.presentToast("End date must greater than start date");
                            this.reset();
                        }
                        else {
                            temp = [];
                            for (a = 0; a < this.reward.length; a++) {
                                if ((this.reward[a].date >= this.startdate) && (this.reward[a].date <= this.enddate)) {
                                    temp.push(this.reward[a]);
                                }
                            }
                            this.reward = temp;
                            this.choose_company = 0;
                            this.count_page();
                        }
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    RewardPage.prototype.count_page = function () {
        this.total_page = (this.reward.length) / 10;
        if ((this.reward.length) % 10 != 0) {
            this.total_page = this.total_page + 1;
        }
        this.current_page = 1;
        this.total_page = Number(this.total_page.toFixed());
        this.get_page();
    };
    RewardPage.prototype.get_page = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, temp, max, min, a;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        temp = [];
                        max = this.current_page * 10;
                        min = max - 10;
                        for (a = min; a < max; a++) {
                            if (this.reward[a] != null) {
                                temp.push(this.reward[a]);
                            }
                        }
                        this.display = temp;
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    RewardPage.prototype.filter_company = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, temp, a;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.startdate = "2019-01-01";
                        this.enddate = ((new Date().toISOString()).split("T"))[0];
                        return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        if (this.choose_company != 0) {
                            temp = [];
                            for (a = 0; a < this.data.reward.length; a++) {
                                if (this.data.reward[a].compid == this.choose_company) {
                                    temp.push(this.data.reward[a]);
                                }
                            }
                            this.reward = temp;
                            this.count_page();
                        }
                        else {
                            this.reset();
                        }
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    RewardPage.prototype.get_company = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, temp, a, b;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        temp = [];
                        for (a = 0; a < this.reward.length; a++) {
                            b = 0;
                            for (; b < temp.length; b++) {
                                if (this.reward[a]["compid"] == temp[b]["compid"]) {
                                    break;
                                }
                            }
                            if (b == temp.length) {
                                temp.push(this.reward[a]);
                            }
                        }
                        this.company_list = temp;
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    RewardPage.prototype.company_name = function (compid) {
        for (var a = 0; a < this.data.company.length; a++) {
            if (this.data.company[a].id == compid) {
                return this.data.company[a]["title"];
            }
        }
    };
    RewardPage.prototype.coming = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Stay Tuned!',
                            message: 'More features coming soon',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RewardPage = tslib_1.__decorate([
        Component({
            selector: 'app-reward',
            templateUrl: './reward.page.html',
            styleUrls: ['./reward.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AlertController,
            LoadingController,
            DataService])
    ], RewardPage);
    return RewardPage;
}());
export { RewardPage };
//# sourceMappingURL=reward.page.js.map