import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GoogleMaps, GoogleMapsEvent, LatLng } from '@ionic-native/google-maps';
import { LoadingController, Platform } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { DataService } from '../../providers/data/data.service';
var LocationPage = /** @class */ (function () {
    function LocationPage(plt, loadctrl, navCtrl, data) {
        var _this = this;
        this.plt = plt;
        this.loadctrl = loadctrl;
        this.navCtrl = navCtrl;
        this.data = data;
        this.status = "not click";
        this.plt.ready().then(function () {
            _this.loadMap();
        });
    }
    LocationPage.prototype.ngOnInit = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    LocationPage.prototype.loadMap = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadctrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.map = GoogleMaps.create('map_canvas');
                        return [4 /*yield*/, this.map.one(GoogleMapsEvent.MAP_READY).then(function () {
                                var options = { timeout: 20000 };
                                /* Geolocation.getCurrentPosition().then(async (resp) => {
                                    let loading = await this.loadctrl.create();
                                    await loading.present();
                                    let loc : LatLng = new LatLng(resp.coords.latitude,resp.coords.longitude);
                                    await this.moveCamera(loc);
                                    let pos : LatLng = new LatLng(resp.coords.latitude,resp.coords.longitude);
                                    await this.ownMarker(pos,"Your Location").then((marker:Marker)=>{ marker.showInfoWindow(); })
                                    this.display = "yes";
                                    loading.dismiss();
                                 }).catch(async (error) => {
                                    let loading = await this.loadctrl.create();
                                    await loading.present();
                                    let loc : LatLng = new LatLng(3.044677,101.446837);
                                    this.moveCamera(loc);
                                    this.display ="no";
                                    loading.dismiss();
                                 });*/
                                var loc = new LatLng(3.044677, 101.446837);
                                _this.moveCamera(loc);
                                /*   let pos : LatLng = new LatLng(3.086029,101.443148);
                                  this.createMarker(pos,"Deja Woof Cafe","3").then((marker:Marker)=>{ }) */
                                for (var a = 0; a < _this.data.company.length; a++) {
                                    var pos = new LatLng(_this.data.company[a]["lat"], _this.data.company[a]["lng"]);
                                    _this.createMarker(pos, _this.data.company[a]["title"] + "  ( " + _this.data.company[a]["label"] + " receipt )")
                                        .then(function (marker) {
                                        marker.addEventListener(GoogleMapsEvent.INFO_CLICK).subscribe(function (e) {
                                            //this.status=this.data[a]["title"];
                                            _this.navCtrl.navigateForward('receipt');
                                        });
                                    });
                                }
                                //this.locate();
                            }, function (error) { loading.dismiss(); })];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, loading.dismiss()];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /*async locate(){
        let loading = await this.loadctrl.create();
        await loading.present();
        this.geolocation.getCurrentPosition().then((resp) => {
          let loc : LatLng = new LatLng(resp.coords.latitude,resp.coords.longitude);
          this.moveCamera(loc);
          let pos : LatLng = new LatLng(resp.coords.latitude,resp.coords.longitude);
          this.ownMarker(pos,"Your Location").then((marker:Marker)=>{ marker.showInfoWindow(); })
          this.display = "yes";
        }).catch(async (error) => {
          let loc : LatLng = new LatLng(3.044677,101.446837);
          this.moveCamera(loc);
          this.display ="no";
        });
        await loading.dismiss();
    }*/
    LocationPage.prototype.moveCamera = function (loc) {
        var options = { target: loc, zoom: 11, tilt: 10 };
        this.map.moveCamera(options);
    };
    LocationPage.prototype.createMarker = function (loc, title) {
        var markerOptions = { position: loc, title: title,
            icon: 'http://labs.google.com/ridefinder/images/mm_20_blue.png',
        };
        return this.map.addMarker(markerOptions);
    };
    LocationPage.prototype.ownMarker = function (loc, title) {
        var markerOptions = { position: loc, title: title,
            icon: 'http://labs.google.com/ridefinder/images/mm_20_red.png',
        };
        return this.map.addMarker(markerOptions);
    };
    LocationPage.prototype.click_window = function (ttl) {
        this.status = ttl;
    };
    LocationPage = tslib_1.__decorate([
        Component({
            selector: 'app-location',
            templateUrl: './location.page.html',
            styleUrls: ['./location.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            LoadingController,
            NavController,
            DataService])
    ], LocationPage);
    return LocationPage;
}());
export { LocationPage };
//# sourceMappingURL=location.page.js.map