import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
var WalletPage = /** @class */ (function () {
    function WalletPage(navCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    WalletPage.prototype.ngOnInit = function () {
    };
    WalletPage.prototype.ionViewWillEnter = function () {
    };
    WalletPage = tslib_1.__decorate([
        Component({
            selector: 'app-wallet',
            templateUrl: './wallet.page.html',
            styleUrls: ['./wallet.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, LoadingController])
    ], WalletPage);
    return WalletPage;
}());
export { WalletPage };
//# sourceMappingURL=wallet.page.js.map