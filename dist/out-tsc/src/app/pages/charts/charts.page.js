import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import chartJs from 'chart.js';
import { NavController, MenuController, AlertController, Events } from '@ionic/angular';
import { TranslateProvider, HotelProvider, AuthProvider, UserProvider } from '../../providers';
var ChartsPage = /** @class */ (function () {
    //   doughnutChart: any;
    function ChartsPage(navCtrl, menuCtrl, translate, hotels, authService, userService, alertCtrl, events) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.translate = translate;
        this.hotels = hotels;
        this.authService = authService;
        this.userService = userService;
        this.alertCtrl = alertCtrl;
        this.events = events;
    }
    ChartsPage.prototype.ionViewWillEnter = function () {
        this.getUser();
    };
    ChartsPage.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.barChart = _this.getBarChart(_this.id);
        }, 450);
    };
    ChartsPage.prototype.getUser = function () {
        var _this = this;
        this.userService.getUserInfo()
            .then(function (response) {
            //   console.log(response);
            _this.user = response;
            _this.id = response._id;
            _this.getBarChart(_this.id);
        })
            .catch(function (err) {
            _this.navCtrl.navigateRoot('authentication');
        });
    };
    ChartsPage.prototype.ionViewCanEnter = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isAuthenticated;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.authService.checkIsAuthenticated()];
                    case 1:
                        isAuthenticated = _a.sent();
                        return [2 /*return*/, isAuthenticated];
                }
            });
        });
    };
    ChartsPage.prototype.checkAuthenticated = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isAuthenticated, err_1, alert_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 5]);
                        return [4 /*yield*/, this.authService.checkIsAuthenticated()];
                    case 1:
                        isAuthenticated = _a.sent();
                        if (isAuthenticated) {
                            this.menuCtrl.enable(true);
                            this.navCtrl.navigateForward('/home');
                        }
                        return [3 /*break*/, 5];
                    case 2:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [4 /*yield*/, this.alertCtrl.create({ header: 'Error', message: 'Error on verify authentication info', buttons: ['Ok'] })];
                    case 3:
                        alert_1 = _a.sent();
                        return [4 /*yield*/, alert_1.present()];
                    case 4:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    ChartsPage.prototype.getChart = function (context, chartType, data, options) {
        return new chartJs(context, {
            data: data,
            options: options,
            type: chartType,
        });
    };
    ChartsPage.prototype.getBarChart = function (id) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            message: 'Retrieving data, please wait...',
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        this.userService.getHomeChart(id)
                            .then(function (response) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var Result, monthNames, itemMonths, itemCountList, start, month, year, date, i, months, monthValue, dataObj, data, options;
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, alert.dismiss()];
                                    case 1:
                                        _a.sent();
                                        Result = [];
                                        Object.keys(response).forEach(function (key) {
                                            Result.push({
                                                Months: key,
                                                Counts: Math.round((response[key]) * 100) / 100
                                            });
                                        });
                                        monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                                            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                                        ];
                                        itemMonths = [], itemCountList = [];
                                        date = new Date();
                                        month = date.getMonth();
                                        year = parseInt('2019');
                                        start = 0;
                                        for (i = 0; i < 12; i++) {
                                            months = monthNames[start];
                                            monthValue = 0;
                                            itemMonths.push(months);
                                            start = start + 1;
                                            if (start == 12) {
                                                start = 0;
                                                year = year + 1;
                                            }
                                            dataObj = $.grep(Result, function (a) {
                                                return a.Months === months;
                                            })[0];
                                            monthValue = dataObj !== undefined ? dataObj.Counts : 0;
                                            itemCountList.push(monthValue);
                                        }
                                        data = {
                                            labels: itemMonths,
                                            datasets: [{
                                                    label: 'Total expenditure in ' + date.getFullYear() + ' (RM)',
                                                    data: itemCountList,
                                                    backgroundColor: [
                                                        'rgba(255, 99, 132)',
                                                        'rgba(54, 162, 235)',
                                                        'rgba(255, 206, 86)',
                                                        'rgba(75, 192, 192)',
                                                        'rgba(153, 102, 255)',
                                                        'rgba(255, 159, 64)',
                                                        'rgba(255, 99, 132)',
                                                        'rgba(54, 162, 235)',
                                                        'rgba(255, 206, 86)',
                                                        'rgba(75, 192, 192)',
                                                        'rgba(153, 102, 255)',
                                                        'rgba(255, 159, 64)'
                                                    ],
                                                    borderColor: [
                                                        'rgba(255,99,132,1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(255, 206, 86, 1)',
                                                        'rgba(75, 192, 192, 1)',
                                                        'rgba(153, 102, 255, 1)',
                                                        'rgba(255, 159, 64, 1)',
                                                        'rgba(255,99,132,1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(255, 206, 86, 1)',
                                                        'rgba(75, 192, 192, 1)',
                                                        'rgba(153, 102, 255, 1)',
                                                        'rgba(255, 159, 64, 1)'
                                                    ],
                                                    borderWidth: 3
                                                }]
                                        };
                                        options = {
                                            scales: {
                                                yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                            }
                                        };
                                        return [2 /*return*/, this.getChart(this.barCanvas.nativeElement, 'bar', data, options)];
                                }
                            });
                        }); }).catch(function (err) {
                            var alert = _this.alertCtrl.create({ header: 'Error', message: 'Error searching', buttons: ['Ok'] });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    tslib_1.__decorate([
        ViewChild('barCanvas'),
        tslib_1.__metadata("design:type", Object)
    ], ChartsPage.prototype, "barCanvas", void 0);
    ChartsPage = tslib_1.__decorate([
        Component({
            selector: 'app-charts',
            templateUrl: './charts.page.html',
            styleUrls: ['./charts.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            TranslateProvider,
            HotelProvider,
            AuthProvider,
            UserProvider,
            AlertController,
            Events])
    ], ChartsPage);
    return ChartsPage;
}());
export { ChartsPage };
//# sourceMappingURL=charts.page.js.map