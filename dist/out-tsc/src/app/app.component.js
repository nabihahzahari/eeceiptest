import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform, NavController, AlertController, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateProvider } from './providers/translate/translate.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import { AuthProvider, UserProvider } from './providers';
/**
 * Main Wrap App Component with starting methods
 *
 * @export
 * @class AppComponent
 */
var AppComponent = /** @class */ (function () {
    /**
     * Creates an instance of AppComponent.
     * @param {Platform} platform
     * @param {SplashScreen} splashScreen
     * @param {StatusBar} statusBar
     * @param {TranslateProvider} translate
     * @param {TranslateService} translateService
     * @param {NavController} navCtrl
     * @memberof AppComponent
     */
    function AppComponent(platform, splashScreen, statusBar, translate, translateService, navCtrl, alertCtrl, authService, events, userService) {
        var _this = this;
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.translate = translate;
        this.translateService = translateService;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.authService = authService;
        this.events = events;
        this.userService = userService;
        this.events.subscribe('user', function (response) {
            // user and time are the same arguments passed in `events.publish(user, time)`
            // this.user.id = data.id;
            // console.log(response);
            //  console.log('Welcome', response.name);
            _this.user = response;
            //  console.log(response);
        });
        this.appPages = [
            {
                title: 'Home',
                url: '/home',
                direct: 'root',
                icon: 'home'
            },
            {
                title: 'Receipt',
                url: '/receipt',
                direct: 'forward',
                icon: 'paper'
            },
            {
                title: 'Analytic',
                url: '/charts',
                direct: 'forward',
                icon: 'pie'
            },
            {
                title: 'Reward',
                url: '/reward',
                direct: 'forward',
                icon: 'medal'
            },
            {
                title: 'Location',
                url: '/location',
                direct: 'forward',
                icon: 'pin'
            },
            //   {
            //     title: 'Wallet',
            //     url: '/wallet',
            //     direct: 'forward',
            //     icon: 'wallet'
            //   },
            {
                title: 'About',
                url: '/about',
                direct: 'forward',
                icon: 'information-circle-outline'
            },
            {
                title: 'Support',
                url: '/support',
                direct: 'forward',
                icon: 'help-buoy'
            }
        ];
        this.initializeApp();
    }
    /**
     * Method that starts all Cordova and Factories
     *
     * @memberof AppComponent
     */
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            setTimeout(function () {
                _this.splashScreen.hide();
            }, 1000);
            // Set language of the app.
            _this.translateService.setDefaultLang(environment.language);
            _this.translateService.use(environment.language);
            _this.translateService.getTranslation(environment.language).subscribe(function (translations) {
                _this.translate.setTranslations(translations);
            });
        }).catch(function () {
            // Set language of the app.
            _this.translateService.setDefaultLang(environment.language);
            _this.translateService.use(environment.language);
            _this.translateService.getTranslation(environment.language).subscribe(function (translations) {
                _this.translate.setTranslations(translations);
            });
        });
    };
    /**
     * Navigate to Edit Profile Page
     *
     * @memberof AppComponent
     */
    AppComponent.prototype.goToEditProgile = function () {
        this.navCtrl.navigateForward('edit-profile');
    };
    /**
     * Logout Method
     *
     * @memberof AppComponent
     */
    AppComponent.prototype.logout = function () {
        var _this = this;
        this.authService.removeCredentials();
        setTimeout(function () {
            // this.menuCtrl.enable(true);
            _this.navCtrl.navigateForward('/authentication');
        }, 750);
    };
    AppComponent.prototype.coming = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Stay Tuned!',
                            //   subHeader: 'Subtitle',
                            message: 'More features coming soon',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html',
            styleUrls: ['./app.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            SplashScreen,
            StatusBar,
            TranslateProvider,
            TranslateService,
            NavController,
            AlertController,
            AuthProvider,
            Events,
            UserProvider])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map