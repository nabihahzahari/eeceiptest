import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { activities } from './mock-activities';
var ActivitiesService = /** @class */ (function () {
    function ActivitiesService() {
        this.activities = activities;
    }
    ActivitiesService.prototype.getAll = function () {
        return this.activities;
    };
    ActivitiesService.prototype.getItem = function (id) {
        for (var i = 0; i < this.activities.length; i++) {
            if (this.activities[i].id === parseInt(id)) {
                return this.activities[i];
            }
        }
        return null;
    };
    ActivitiesService.prototype.remove = function (item) {
        this.activities.splice(this.activities.indexOf(item), 1);
    };
    ActivitiesService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ActivitiesService);
    return ActivitiesService;
}());
export { ActivitiesService };
//# sourceMappingURL=activities.service.js.map