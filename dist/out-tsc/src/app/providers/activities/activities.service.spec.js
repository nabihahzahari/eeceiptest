import { TestBed, inject } from '@angular/core/testing';
import { ActivitiesService } from './activities.service';
describe('ActivitiesService', function () {
    beforeEach(function () {
        TestBed.configureTestingModule({
            providers: [ActivitiesService]
        });
    });
    it('should be created', inject([ActivitiesService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=activities.service.spec.js.map