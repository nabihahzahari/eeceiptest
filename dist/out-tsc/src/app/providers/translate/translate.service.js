import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var TranslateProvider = /** @class */ (function () {
    function TranslateProvider() {
    }
    // Set the translations of the app.
    TranslateProvider.prototype.setTranslations = function (translations) {
        this.translations = translations;
    };
    TranslateProvider.prototype.getTranslations = function () {
        return this.translations;
    };
    // Get the translated string given the key based on the i18n .json file.
    TranslateProvider.prototype.get = function (key) {
        return this.translations[key];
    };
    TranslateProvider = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], TranslateProvider);
    return TranslateProvider;
}());
export { TranslateProvider };
//# sourceMappingURL=translate.service.js.map