import { TestBed, inject } from '@angular/core/testing';
import { CarsService } from './cars.service';
describe('CarsService', function () {
    beforeEach(function () {
        TestBed.configureTestingModule({
            providers: [CarsService]
        });
    });
    it('should be created', inject([CarsService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=cars.service.spec.js.map