import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { CARS } from './mock-cars';
var CarsService = /** @class */ (function () {
    function CarsService() {
        this.cars = CARS;
    }
    CarsService.prototype.getAll = function () {
        return this.cars;
    };
    CarsService.prototype.getItem = function (id) {
        for (var i = 0; i < this.cars.length; i++) {
            if (this.cars[i].id === parseInt(id)) {
                return this.cars[i];
            }
        }
        return null;
    };
    CarsService.prototype.getCar = function (carshopID, carID) {
        var carshop = this.getItem(carshopID);
        for (var i = 0; i < carshop.cars.length; i++) {
            if (carshop.cars[i].id === parseInt(carID)) {
                return carshop.cars[i];
            }
        }
        return null;
    };
    CarsService.prototype.remove = function (item) {
        this.cars.splice(this.cars.indexOf(item), 1);
    };
    CarsService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], CarsService);
    return CarsService;
}());
export { CarsService };
//# sourceMappingURL=cars.service.js.map