import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { HtmlPipe } from './html.pipe';
var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                HtmlPipe,
            ],
            imports: [],
            exports: [
                HtmlPipe
            ]
        })
    ], PipesModule);
    return PipesModule;
}());
export { PipesModule };
//# sourceMappingURL=pipe.module.js.map