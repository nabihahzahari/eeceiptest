import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
var HtmlPipe = /** @class */ (function () {
    function HtmlPipe(dom) {
        this.dom = dom;
    }
    HtmlPipe.prototype.transform = function (value, args) {
        return this.dom.bypassSecurityTrustHtml(value);
    };
    HtmlPipe = tslib_1.__decorate([
        Pipe({
            name: 'html'
        }),
        tslib_1.__metadata("design:paramtypes", [DomSanitizer])
    ], HtmlPipe);
    return HtmlPipe;
}());
export { HtmlPipe };
//# sourceMappingURL=html.pipe.js.map