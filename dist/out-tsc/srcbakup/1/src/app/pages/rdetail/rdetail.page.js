import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, NavParams, ModalController, LoadingController } from '@ionic/angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { ApiService } from '../../api.service';
import { RatePage } from '../rate/rate.page';
import { DomSanitizer } from '@angular/platform-browser';
import * as html2canvas from 'html2canvas';
var RdetailPage = /** @class */ (function () {
    //imag:any;
    function RdetailPage(modalController, router, navParams, 
    //private emailComposer: EmailComposer,
    loadCtrl, apiservice, platform, sanitizer) {
        this.modalController = modalController;
        this.router = router;
        this.navParams = navParams;
        this.loadCtrl = loadCtrl;
        this.apiservice = apiservice;
        this.platform = platform;
        this.sanitizer = sanitizer;
        this.star = 4;
        this.display = "koko";
        this.displa = "mula";
        this.datas = "jur";
    }
    RdetailPage.prototype.now = function () {
        var _this = this;
        //this.apiservice.getdata("test1").subscribe( (res)=>{this.display=res;}, (err)=>{this.display="takdapat";} )
        this.apiservice.postdata("test2", {}).subscribe(function (res) { _this.display = res; }, function (err) { _this.display = "takdapat"; });
    };
    RdetailPage.prototype.ngOnInit = function () { };
    RdetailPage.prototype.ionViewWillEnter = function () {
        // console.table(this.navParams);
        this.modelId = this.sanitizer.bypassSecurityTrustHtml(this.navParams.data.html);
        this.modalTitle = this.navParams.data.receiptId;
        this.modalstar = this.navParams.data.star;
        if (this.navParams.data.star == null) {
            this.displa = "null"; //this.displayrate(); 
        }
        else {
            this.displa = "taknull";
        }
    };
    RdetailPage.prototype.ionViewDidEnter = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, data;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        data = document.getElementById('rec');
                        return [4 /*yield*/, html2canvas(data).then(function (canvas) {
                                var contentDataURL = canvas.toDataURL('image/png');
                                _this.imag = "base64:receipt.png//" + contentDataURL.substr(22);
                            })];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, loading.dismiss()];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RdetailPage.prototype.closeModal = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var onClosedData;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        onClosedData = "Wrapped Up!";
                        return [4 /*yield*/, this.modalController.dismiss(onClosedData)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RdetailPage.prototype.back = function () { this.router.navigateByUrl('/receipt'); };
    RdetailPage.prototype.coming = function () { alert("more features are coming soon"); };
    RdetailPage.prototype.email = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, email;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        email = {
                            to: '', cc: '', bcc: '', attachments: [this.imag],
                            subject: 'My Eeceipt Copy',
                            body: '<br>Thank you for using Eeceipt.<br>Please download our <a href="linked">mobile app</a> to save your receipts and  enjoy future discounts & rewards from our partners',
                            isHtml: true
                        };
                        return [4 /*yield*/, EmailComposer.open(email).then(function () { }).catch(function (err) { alert("No email app found"); })];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, loading.dismiss()];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RdetailPage.prototype.displayrate = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var loading, modal;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, loading.dismiss()];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.modalController.create({
                                component: RatePage,
                                cssClass: "rate-modal",
                                componentProps: {
                                    'star': this.modalstar
                                }
                            })];
                    case 4:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (data) {
                            if (data["data"] != null) {
                                _this.modalstar = data["data"];
                                _this.displa = data["data"];
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 5: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    RdetailPage = tslib_1.__decorate([
        Component({
            selector: 'app-rdetail',
            templateUrl: './rdetail.page.html',
            styleUrls: ['./rdetail.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ModalController,
            Router,
            NavParams,
            LoadingController,
            ApiService,
            Platform,
            DomSanitizer])
    ], RdetailPage);
    return RdetailPage;
}());
export { RdetailPage };
//# sourceMappingURL=rdetail.page.js.map