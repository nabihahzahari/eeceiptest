import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RdetailPage } from './rdetail.page';
import { PipesModule } from '../../../pipes/pipe.module';
import { PinchZoomModule } from 'ngx-pinch-zoom';
var routes = [
    {
        path: '',
        component: RdetailPage
    }
];
var RdetailPageModule = /** @class */ (function () {
    function RdetailPageModule() {
    }
    RdetailPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                PipesModule,
                PinchZoomModule,
                RouterModule.forChild(routes)
            ],
            declarations: [RdetailPage]
        })
    ], RdetailPageModule);
    return RdetailPageModule;
}());
export { RdetailPageModule };
//# sourceMappingURL=rdetail.module.js.map