import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var PopupmenuPage = /** @class */ (function () {
    function PopupmenuPage() {
    }
    PopupmenuPage.prototype.ngOnInit = function () {
    };
    PopupmenuPage = tslib_1.__decorate([
        Component({
            selector: 'app-popupmenu',
            templateUrl: './popupmenu.page.html',
            styleUrls: ['./popupmenu.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], PopupmenuPage);
    return PopupmenuPage;
}());
export { PopupmenuPage };
//# sourceMappingURL=popupmenu.page.js.map