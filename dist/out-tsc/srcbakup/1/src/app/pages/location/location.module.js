import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LocationPage } from './location.page';
import { PopupmenuPageModule } from '../popupmenu/popupmenu.module';
import { TranslateModule } from '@ngx-translate/core';
var routes = [
    {
        path: '',
        component: LocationPage
    }
];
var LocationPageModule = /** @class */ (function () {
    function LocationPageModule() {
    }
    LocationPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                PopupmenuPageModule,
                TranslateModule.forChild(),
                RouterModule.forChild(routes)
            ],
            declarations: [LocationPage]
        })
    ], LocationPageModule);
    return LocationPageModule;
}());
export { LocationPageModule };
//# sourceMappingURL=location.module.js.map