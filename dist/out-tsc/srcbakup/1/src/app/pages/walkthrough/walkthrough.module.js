import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { WalkthroughPage } from './walkthrough.page';
var routes = [
    {
        path: '',
        component: WalkthroughPage
    }
];
var WalkthroughPageModule = /** @class */ (function () {
    function WalkthroughPageModule() {
    }
    WalkthroughPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [WalkthroughPage]
        })
    ], WalkthroughPageModule);
    return WalkthroughPageModule;
}());
export { WalkthroughPageModule };
//# sourceMappingURL=walkthrough.module.js.map