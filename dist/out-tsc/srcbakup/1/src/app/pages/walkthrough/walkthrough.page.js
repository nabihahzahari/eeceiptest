import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, IonSlides, MenuController, AlertController } from '@ionic/angular';
var WalkthroughPage = /** @class */ (function () {
    function WalkthroughPage(navCtrl, menuCtrl, router, alertCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.showSkip = true;
        this.slideOpts = { effect: 'flip', speed: 1000 };
        this.slideList = [
            { title: 'What is <strong>Eeceipt</strong>?',
                description: 'We believe in transforming the traditional paper receipt',
                image: 'assets/img/hotel-sp01.png',
            },
            { title: 'Why Eeceipt?',
                description: 'Digitizing receipt will unlock multiple values for both retailer and customers.',
                image: 'assets/img/hotel-sp02.png',
            },
            { title: 'Your Eeceipt is coming!',
                description: ' Just scan the QR Code on the Simplified Eeceipt to unlock the level of details in the traditional paper receipt.',
                image: 'assets/img/qrcode-512.png',
            }
        ];
    }
    WalkthroughPage.prototype.ionViewWillEnter = function () { this.menuCtrl.enable(false); };
    WalkthroughPage.prototype.ngOnInit = function () { };
    WalkthroughPage.prototype.onSlideNext = function () { this.slides.slideNext(1000, false); };
    WalkthroughPage.prototype.onSlidePrev = function () { this.slides.slidePrev(300); };
    WalkthroughPage.prototype.openLoginPage = function () { this.navCtrl.navigateForward('/authentication'); };
    tslib_1.__decorate([
        ViewChild(IonSlides),
        tslib_1.__metadata("design:type", IonSlides)
    ], WalkthroughPage.prototype, "slides", void 0);
    WalkthroughPage = tslib_1.__decorate([
        Component({
            selector: 'app-walkthrough',
            templateUrl: './walkthrough.page.html',
            styleUrls: ['./walkthrough.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController,
            Router,
            AlertController])
    ], WalkthroughPage);
    return WalkthroughPage;
}());
export { WalkthroughPage };
//# sourceMappingURL=walkthrough.page.js.map