import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var SupportPage = /** @class */ (function () {
    function SupportPage() {
    }
    SupportPage.prototype.ngOnInit = function () {
    };
    SupportPage = tslib_1.__decorate([
        Component({
            selector: 'app-support',
            templateUrl: './support.page.html',
            styleUrls: ['./support.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SupportPage);
    return SupportPage;
}());
export { SupportPage };
//# sourceMappingURL=support.page.js.map