import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, LoadingController, AlertController, Events } from '@ionic/angular';
import { TranslateProvider, AuthProvider, UserProvider } from '../../providers';
var HomePage = /** @class */ (function () {
    function HomePage(menuCtrl, loadingCtrl, translate, authService, userService, alertCtrl, events, navCtrl) {
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.translate = translate;
        this.authService = authService;
        this.userService = userService;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.navCtrl = navCtrl;
    }
    HomePage.prototype.ngAfterViewInit = function () {
    };
    HomePage.prototype.ngOnInit = function () {
    };
    HomePage.prototype.ionViewWillEnter = function () {
        this.getUser();
        this.menuCtrl.enable(true);
    };
    HomePage.prototype.getUser = function () {
        var _this = this;
        this.userService.getUserInfo()
            .then(function (response) {
            _this.user = response;
            _this.id = response._id;
            _this.value = response._id;
            _this.events.publish('user', _this.user);
        })
            .catch(function (err) {
            _this.navCtrl.navigateRoot('authentication');
        });
    };
    HomePage.prototype.ionViewCanEnter = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isAuthenticated;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.authService.checkIsAuthenticated()];
                    case 1:
                        isAuthenticated = _a.sent();
                        return [2 /*return*/, isAuthenticated];
                }
            });
        });
    };
    HomePage.prototype.editprofile = function () {
        this.navCtrl.navigateForward('edit-profile');
    };
    HomePage.prototype.checkAuthenticated = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var isAuthenticated, err_1, alert_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 5]);
                        return [4 /*yield*/, this.authService.checkIsAuthenticated()];
                    case 1:
                        isAuthenticated = _a.sent();
                        if (isAuthenticated) {
                            this.menuCtrl.enable(true);
                            this.navCtrl.navigateForward('/home');
                        }
                        return [3 /*break*/, 5];
                    case 2:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [4 /*yield*/, this.alertCtrl.create({ header: 'Error', message: 'Error on verify authentication info', buttons: ['Ok'] })];
                    case 3:
                        alert_1 = _a.sent();
                        return [4 /*yield*/, alert_1.present()];
                    case 4:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    tslib_1.__decorate([
        ViewChild('barCanvas'),
        tslib_1.__metadata("design:type", Object)
    ], HomePage.prototype, "barCanvas", void 0);
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: 'home.page.html',
            styleUrls: ['home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuController,
            LoadingController,
            TranslateProvider,
            AuthProvider,
            UserProvider,
            AlertController,
            Events,
            NavController])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map