import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { AboutPage } from './about.page';
// import { PopmenuComponent } from './../../components/popmenu/popmenu.component';
import { PopupmenuPageModule } from '../popupmenu/popupmenu.module';
var routes = [
    {
        path: '',
        component: AboutPage
    }
];
var AboutPageModule = /** @class */ (function () {
    function AboutPageModule() {
    }
    AboutPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                PopupmenuPageModule,
                TranslateModule.forChild(),
                RouterModule.forChild(routes)
            ],
            declarations: [AboutPage]
        })
    ], AboutPageModule);
    return AboutPageModule;
}());
export { AboutPageModule };
//# sourceMappingURL=about.module.js.map