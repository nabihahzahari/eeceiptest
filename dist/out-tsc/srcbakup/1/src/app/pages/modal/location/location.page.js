import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
var LocationPage = /** @class */ (function () {
    function LocationPage(navCtrl, modalCtrl, storage) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        // places
        this.places = {
            nearby: [
                {
                    id: 1,
                    name: "Current Location"
                },
                {
                    id: 2,
                    name: "Rio de Janeiro, Brazil"
                },
                {
                    id: 3,
                    name: "São Paulo, Brazil"
                },
                {
                    id: 4,
                    name: "New York, United States"
                },
                {
                    id: 5,
                    name: "London, United Kingdom"
                },
                {
                    id: 6,
                    name: "Same as pickup"
                }
            ],
            recent: [
                {
                    id: 1,
                    name: "Rio de Janeiro"
                }
            ]
        };
        this.searchterm = '';
    }
    LocationPage.prototype.ngOnInit = function () {
        console.log(this.fromto);
        this.results = this.places.nearby;
        console.log(this.results);
    };
    LocationPage.prototype.setFilteredItems = function (ev) {
        var val = ev ? ev.target.value : '';
        // if the value is an empty string don't filter the items
        if (val && val.trim() !== '') {
            // this.showItems = true;
            this.results = this.places.nearby.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
        else {
            this.results = this.places.nearby;
        }
        // return this.places.nearby.filter((item) => {
        //   return item.name.includes(term);
        // });  
    };
    LocationPage.prototype.searchBy = function (item) {
        if (this.fromto === 'from') {
            this.storage.set('pickup', item.name);
            this.search.pickup = item.name;
        }
        if (this.fromto === 'to') {
            this.storage.set('dropOff', item.name);
            this.search.dropOff = item.name;
        }
        this.modalCtrl.dismiss();
    };
    LocationPage.prototype.closeModal = function () {
        this.modalCtrl.dismiss();
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], LocationPage.prototype, "fromto", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], LocationPage.prototype, "search", void 0);
    LocationPage = tslib_1.__decorate([
        Component({
            selector: 'app-location',
            templateUrl: './location.page.html',
            styleUrls: ['./location.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            ModalController,
            Storage])
    ], LocationPage);
    return LocationPage;
}());
export { LocationPage };
//# sourceMappingURL=location.page.js.map