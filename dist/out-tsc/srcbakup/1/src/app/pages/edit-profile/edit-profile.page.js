import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController, Events, AlertController } from '@ionic/angular';
import { TranslateProvider, AuthProvider, UserProvider } from '../../providers';
import { FormBuilder } from '@angular/forms';
var EditProfilePage = /** @class */ (function () {
    function EditProfilePage(navCtrl, loadingCtrl, toastCtrl, translate, events, authService, userService, alertCtrl, formBuilder) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.translate = translate;
        this.events = events;
        this.authService = authService;
        this.userService = userService;
        this.alertCtrl = alertCtrl;
        this.formBuilder = formBuilder;
    }
    EditProfilePage.prototype.ngOnInit = function () {
    };
    EditProfilePage.prototype.ionViewWillEnter = function () {
        this.getUser();
        this.inDev();
    };
    EditProfilePage.prototype.ionViewWillLeave = function () {
    };
    EditProfilePage.prototype.getUser = function () {
        var _this = this;
        // this.loading = true;
        this.userService.getUserInfo()
            .then(function (response) {
            // this.loading = false;
            _this.user = response;
            // console.log(this.user);
            //  console.log(this.user);
            // this.events.publish('user', this.user);
        })
            .catch(function (err) {
            _this.navCtrl.navigateRoot('authentication');
            //   console.log('error');
            // this.loading = false;
            // let alert = this.alertCtrl.create({ title: 'Error', message: 'Error on get user info', buttons: ['Ok'] });
            // alert.present();
        });
    };
    //   s () {
    //     this.events.subscribe('userss', (response) => {
    //         // user and time are the same arguments passed in `events.publish(user, time)`
    //         // this.user.id = data.id;
    //         // console.log(response);
    //          this.user = response;
    //          console.log('wei', this.user.name);
    //       });
    EditProfilePage.prototype.sendData = function (rawUpdate) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                console.log(rawUpdate);
                return [2 /*return*/];
            });
        });
    };
    EditProfilePage.prototype.coming = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Stay Tuned!',
                            //   subHeader: 'Subtitle',
                            message: 'More features coming soon',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EditProfilePage.prototype.inDev = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'We are sorry',
                            message: 'Page under construction',
                            //   inputs: [
                            //     {
                            //       name: 'email',
                            //       type: 'email',
                            //       placeholder: this.translate.get('app.label.email')
                            //     }
                            //   ],
                            buttons: [
                                {
                                    text: 'Okay',
                                    handler: function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                        var loader;
                                        return tslib_1.__generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0: return [4 /*yield*/, this.loadingCtrl.create({
                                                        duration: 100
                                                    })];
                                                case 1:
                                                    loader = _a.sent();
                                                    loader.present();
                                                    return [2 /*return*/];
                                            }
                                        });
                                    }); }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EditProfilePage = tslib_1.__decorate([
        Component({
            selector: 'app-edit-profile',
            templateUrl: './edit-profile.page.html',
            styleUrls: ['./edit-profile.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            LoadingController,
            ToastController,
            TranslateProvider,
            Events,
            AuthProvider,
            UserProvider,
            AlertController,
            FormBuilder])
    ], EditProfilePage);
    return EditProfilePage;
}());
export { EditProfilePage };
//# sourceMappingURL=edit-profile.page.js.map