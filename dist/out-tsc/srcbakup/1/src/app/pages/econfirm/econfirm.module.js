import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { EconfirmPage } from './econfirm.page';
var routes = [
    {
        path: '',
        component: EconfirmPage
    }
];
var EconfirmPageModule = /** @class */ (function () {
    function EconfirmPageModule() {
    }
    EconfirmPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                TranslateModule.forChild(),
                RouterModule.forChild(routes)
            ],
            declarations: [EconfirmPage]
        })
    ], EconfirmPageModule);
    return EconfirmPageModule;
}());
export { EconfirmPageModule };
//# sourceMappingURL=econfirm.module.js.map