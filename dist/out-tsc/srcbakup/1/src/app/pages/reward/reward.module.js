import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RewardPage } from './reward.page';
import { PopupmenuPageModule } from '../popupmenu/popupmenu.module';
var routes = [
    {
        path: '',
        component: RewardPage
    }
];
var RewardPageModule = /** @class */ (function () {
    function RewardPageModule() {
    }
    RewardPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                PopupmenuPageModule,
                RouterModule.forChild(routes)
            ],
            declarations: [RewardPage]
        })
    ], RewardPageModule);
    return RewardPageModule;
}());
export { RewardPageModule };
//# sourceMappingURL=reward.module.js.map