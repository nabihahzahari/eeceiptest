import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavParams, LoadingController, ModalController, ToastController } from '@ionic/angular';
var RatePage = /** @class */ (function () {
    function RatePage(loadctrl, modalctrl, toastctrl, navParams) {
        this.loadctrl = loadctrl;
        this.modalctrl = modalctrl;
        this.toastctrl = toastctrl;
        this.navParams = navParams;
        this.total = 0;
        this.nombo = 0;
        this.display = "";
    }
    RatePage.prototype.ngOnInit = function () {
        if (this.navParams.data.star == null) {
            this.starmodal = 0;
        }
        else {
            this.starmodal = this.navParams.data.star;
        }
    };
    /*async rate(num : number){
      let loading = await this.loadctrl.create();
      await loading.present();
  
      this.star1="norate"; this.star2="norate"; this.star3="norate"; this.star4="norate"; this.star5="norate";
  
      if(num>=0){ this.star1="rated"; }
      if(num>=1){ this.star2="rated"; }
      if(num>=2){ this.star3="rated"; }
      if(num>=3){ this.star4="rated"; }
      if(num>=4){ this.star5="rated"; }
  
      await loading.dismiss();
    }*/
    RatePage.prototype.close = function () { this.modalctrl.dismiss(); };
    RatePage.prototype.submit = function () {
        var _this = this;
        this.modalctrl.dismiss(this.starmodal).then(function () { _this.presentToast(); });
    };
    RatePage.prototype.presentToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastctrl.create({ message: "Thank You For Your Feedback", position: 'middle', duration: 2000 })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    RatePage = tslib_1.__decorate([
        Component({
            selector: 'app-rate',
            templateUrl: './rate.page.html',
            styleUrls: ['./rate.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingController,
            ModalController,
            ToastController,
            NavParams])
    ], RatePage);
    return RatePage;
}());
export { RatePage };
//# sourceMappingURL=rate.page.js.map