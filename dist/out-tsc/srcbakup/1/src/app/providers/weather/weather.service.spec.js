import { TestBed, inject } from '@angular/core/testing';
import { WeatherService } from './weather.service';
describe('WeatherService', function () {
    beforeEach(function () {
        TestBed.configureTestingModule({
            providers: [WeatherService]
        });
    });
    it('should be created', inject([WeatherService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=weather.service.spec.js.map