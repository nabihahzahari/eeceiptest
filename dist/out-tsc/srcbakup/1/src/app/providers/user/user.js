import * as tslib_1 from "tslib";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
// import { Receipt } from '../../models/receipt';
import { Service } from '../../../settings/Laravel';
var UserProvider = /** @class */ (function () {
    function UserProvider(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    UserProvider.prototype.getUserInfo = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var auth, headers, a;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get('auth')];
                    case 1:
                        auth = _a.sent();
                        headers = new HttpHeaders({
                            'Authorization': "Bearer " + auth.access_token,
                        });
                        a = this.http.get(Service.url + "/api/user", { headers: headers }).toPromise();
                        // console.log(a);
                        return [2 /*return*/, a];
                }
            });
        });
    };
    UserProvider.prototype.getScanData = function ($guid, $id) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var auth, headers, getScan;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get('auth')];
                    case 1:
                        auth = _a.sent();
                        headers = new HttpHeaders({
                            'Authorization': "Bearer " + auth.access_token,
                        });
                        getScan = this.http.patch(Service.url + "/api/v1/scan/" + $guid + '/' + $id, { headers: headers }).toPromise();
                        console.log(getScan);
                        return [2 /*return*/, getScan];
                }
            });
        });
    };
    UserProvider.prototype.getReceipt = function (id) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var auth, headers, getR;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get('auth')];
                    case 1:
                        auth = _a.sent();
                        headers = new HttpHeaders({
                            'Authorization': "Bearer " + auth.access_token,
                        });
                        getR = this.http.get(Service.url + "/api/v1/get/" + id, { headers: headers }).toPromise();
                        // console.log(getR);
                        return [2 /*return*/, getR];
                }
            });
        });
    };
    UserProvider.prototype.getHomeChart = function (id) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var auth, headers, getHR;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get('auth')];
                    case 1:
                        auth = _a.sent();
                        headers = new HttpHeaders({
                            'Authorization': "Bearer " + auth.access_token,
                        });
                        getHR = this.http.get(Service.url + "/api/v1/gethomechart/" + id, { headers: headers }).toPromise();
                        //  console.log(getHR);
                        return [2 /*return*/, getHR];
                }
            });
        });
    };
    UserProvider = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient, Storage])
    ], UserProvider);
    return UserProvider;
}());
export { UserProvider };
//# sourceMappingURL=user.js.map