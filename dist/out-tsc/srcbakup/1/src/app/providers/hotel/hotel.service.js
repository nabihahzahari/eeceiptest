import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HOTELS } from "./mock-hotels";
var HotelProvider = /** @class */ (function () {
    function HotelProvider() {
        this.favoriteCounter = 0;
        this.favorites = [];
        this.bookingCounter = 0;
        this.bookings = [];
        this.hotels = HOTELS;
    }
    HotelProvider.prototype.getAll = function () {
        return this.hotels;
    };
    HotelProvider.prototype.getItem = function (id) {
        for (var i = 0; i < this.hotels.length; i++) {
            if (this.hotels[i].id === parseInt(id)) {
                return this.hotels[i];
            }
        }
        return null;
    };
    HotelProvider.prototype.getRoom = function (hotelID, roomID) {
        var hotel = this.getItem(hotelID);
        for (var i = 0; i < hotel.rooms.length; i++) {
            if (hotel.rooms[i].id === parseInt(roomID)) {
                return hotel.rooms[i];
            }
        }
        return null;
    };
    HotelProvider.prototype.remove = function (item) {
        this.hotels.splice(this.hotels.indexOf(item), 1);
    };
    /////
    //For Search and Favorites
    ////
    HotelProvider.prototype.findAll = function () {
        return Promise.resolve(this.hotels);
    };
    HotelProvider.prototype.findById = function (id) {
        return Promise.resolve(this.hotels[id - 1]);
    };
    HotelProvider.prototype.findByName = function (searchKey) {
        var key = searchKey.toUpperCase();
        return Promise.resolve(this.hotels.filter(function (property) {
            return (property.title + ' ' + property.address + ' ' + property.city + ' ' + property.description).toUpperCase().indexOf(key) > -1;
        }));
    };
    HotelProvider.prototype.getFavorites = function () {
        return Promise.resolve(this.favorites);
    };
    HotelProvider.prototype.getBookings = function () {
        return Promise.resolve(this.bookings);
    };
    HotelProvider.prototype.favorite = function (hotel) {
        this.favoriteCounter = this.favoriteCounter + 1;
        var exist = false;
        if (this.favorites && this.favorites.length > 0) {
            this.favorites.forEach(function (val, i) {
                if (val.hotel.id === hotel.id) {
                    exist = true;
                }
            });
        }
        if (!exist) {
            this.favorites.push({ id: this.favoriteCounter, hotel: hotel });
        }
        return Promise.resolve();
    };
    HotelProvider.prototype.booking = function (hotel) {
        this.bookingCounter = this.bookingCounter + 1;
        var existb = false;
        if (this.bookings && this.bookings.length > 0) {
            this.bookings.forEach(function (val, i) {
                if (val.hotel.id === hotel.id) {
                    existb = true;
                }
            });
        }
        if (!existb) {
            this.bookings.push({ id: this.bookingCounter, hotel: hotel });
        }
        return Promise.resolve();
    };
    HotelProvider.prototype.unfavorite = function (favorite) {
        var index = this.favorites.indexOf(favorite);
        if (index > -1) {
            this.favorites.splice(index, 1);
        }
        return Promise.resolve();
    };
    HotelProvider = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], HotelProvider);
    return HotelProvider;
}());
export { HotelProvider };
//# sourceMappingURL=hotel.service.js.map