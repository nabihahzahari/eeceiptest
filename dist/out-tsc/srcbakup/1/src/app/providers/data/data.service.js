import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
var DataService = /** @class */ (function () {
    function DataService(toastctrl) {
        this.toastctrl = toastctrl;
        this.company = [{ "id": 1, "lat": "3.084181", "lng": "101.443238", "title": "RMS Management Solution", "label": "3" },
            { "id": 2, "lat": "3.083966", "lng": "101.447100", "title": "Warung Kita Bukit Raja", "label": "3" },
            { "id": 3, "lat": "3.078878", "lng": "101.438409", "title": "Mahkamah Rendah Shariah Klang", "label": "3" }];
        this.reward = [{ "id": 1, "compid": 1, "title": "Rewards", "date": "2019-07-20", "content": "70% Discount" },
            { "id": 2, "compid": 2, "title": "Rewards", "date": "2019-07-20", "content": "Buy One Free One" },
            { "id": 3, "compid": 3, "title": "Rewards", "date": "2019-07-20", "content": "10% Discount" },
            { "id": 4, "compid": 1, "title": "Rewards", "date": "2019-07-01", "content": "10% Discount" },
            { "id": 5, "compid": 2, "title": "Rewards", "date": "2019-07-01", "content": "Buy One Free One" },
            { "id": 6, "compid": 3, "title": "Rewards", "date": "2019-07-01", "content": "20% Discount" },
            { "id": 7, "compid": 1, "title": "Rewards", "date": "2019-07-01", "content": "30% Discount" },
            { "id": 8, "compid": 2, "title": "Rewards", "date": "2019-07-01", "content": "40% Discount" },
            { "id": 9, "compid": 3, "title": "Rewards", "date": "2019-07-01", "content": "50% Discount" },
            { "id": 10, "compid": 1, "title": "Rewards", "date": "2019-07-01", "content": "60% Discount" },
            { "id": 11, "compid": 2, "title": "Rewards", "date": "2019-07-01", "content": "70% Discount" },
            { "id": 12, "compid": 3, "title": "Rewards", "date": "2019-07-01", "content": "80% Discount" },
            { "id": 13, "compid": 1, "title": "Rewards", "date": "2019-07-01", "content": "90% Discount" }];
        this.receipt = [{ "id": 1, "compid": 1, "date": "2019-07-20", "price": "MYR23.60" },
            { "id": 2, "compid": 2, "date": "2019-07-20", "price": "MYR23.60" },
            { "id": 3, "compid": 3, "date": "2019-07-20", "price": "MYR23.60" },
            { "id": 4, "compid": 1, "date": "2019-07-01", "price": "MYR23.60" },
            { "id": 5, "compid": 2, "date": "2019-07-01", "price": "MYR23.60" },
            { "id": 6, "compid": 3, "date": "2019-07-01", "price": "MYR23.60" },
            { "id": 7, "compid": 1, "date": "2019-07-01", "price": "MYR23.60" },
            { "id": 8, "compid": 2, "date": "2019-07-01", "price": "MYR23.60" },
            { "id": 9, "compid": 3, "date": "2019-07-01", "price": "MYR23.60" },
            { "id": 10, "compid": 1, "date": "2019-07-01", "price": "MYR23.60" },
            { "id": 11, "compid": 2, "date": "2019-07-01", "price": "MYR23.60" },
            { "id": 12, "compid": 3, "date": "2019-07-01", "price": "MYR23.60" },
            { "id": 13, "compid": 1, "date": "2019-07-01", "price": "MYR23.60" }];
    }
    DataService.prototype.presentToast = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastctrl.create({ message: message, position: 'middle', duration: 2000 })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    DataService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [ToastController])
    ], DataService);
    return DataService;
}());
export { DataService };
//# sourceMappingURL=data.service.js.map