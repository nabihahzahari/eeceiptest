import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
var ApiService = /** @class */ (function () {
    function ApiService(http, loadctrl) {
        this.http = http;
        this.loadctrl = loadctrl;
    }
    ApiService.prototype.postdata = function (api, data) {
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json');
        var options = new RequestOptions({ headers: headers });
        return this.http.post("http://localhost:8080/api/" + api, data, options)
            .map(function (res) { return res.json(); });
    };
    ApiService.prototype.getdata = function (api) {
        return this.http.get("http://localhost:8080/api/" + api)
            .map(function (res) { return res.json(); });
    };
    ApiService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Http,
            LoadingController])
    ], ApiService);
    return ApiService;
}());
export { ApiService };
//# sourceMappingURL=api.service.js.map