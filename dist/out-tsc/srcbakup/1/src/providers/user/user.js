import * as tslib_1 from "tslib";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Service } from '../../settings/Laravel';
var UserProvider = /** @class */ (function () {
    function UserProvider(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    UserProvider.prototype.getUserInfo = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var auth, headers;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get('auth')];
                    case 1:
                        auth = _a.sent();
                        headers = new HttpHeaders({
                            'Authorization': "Bearer " + auth.access_token,
                        });
                        return [2 /*return*/, this.http.get(Service.apiUrl + "/user", { headers: headers }).toPromise()];
                }
            });
        });
    };
    UserProvider = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient, Storage])
    ], UserProvider);
    return UserProvider;
}());
export { UserProvider };
//# sourceMappingURL=user.js.map