import * as tslib_1 from "tslib";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Service } from '../../settings/Laravel';
var AuthProvider = /** @class */ (function () {
    function AuthProvider(http, storage) {
        this.http = http;
        this.storage = storage;
        this.isAuthenticated = false;
    }
    AuthProvider.prototype.checkIsAuthenticated = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var now, auth;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        now = Date.now();
                        return [4 /*yield*/, this.storage.get('auth')];
                    case 1:
                        auth = _a.sent();
                        if (!!!auth)
                            return [2 /*return*/, false];
                        if (auth.expired_at <= now)
                            return [2 /*return*/, false];
                        return [2 /*return*/, true];
                }
            });
        });
    };
    AuthProvider.prototype.login = function (user) {
        var request = {
            'grant_type': 'password',
            'client_id': Service.passport.client_id,
            'client_secret': Service.passport.client_secret,
            'username': user.email,
            'password': user.password,
        };
        return this.http.post(Service.url + "/oauth/token", request).toPromise();
    };
    AuthProvider.prototype.register = function (user) {
        return this.http.post(Service.apiUrl + "/register", user).toPromise();
    };
    AuthProvider.prototype.removeCredentials = function () {
        this.storage.remove('auth');
    };
    AuthProvider.prototype.storeCredentials = function (response) {
        var expired_at = (response.expires_in * 1000) + Date.now();
        this.storage.set('auth', {
            access_token: response.access_token,
            refresh_token: response.refresh_token,
            expired_at: expired_at
        });
    };
    AuthProvider = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient, Storage])
    ], AuthProvider);
    return AuthProvider;
}());
export { AuthProvider };
//# sourceMappingURL=auth.js.map